var table = '';
var selectBahanBaku = '';
var selectSatuan = '';
var selectStatus = '';

$('.li-po').addClass('menu-open');
$('.li-po .treeview-menu').css('display', 'block');
$('.li-belanja').addClass('active');

function csrf()
{
    $.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
                $('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

function initSelect2()
{
    $('select[name="status[]"]').select2({
        minimumResultsForSearch: Infinity
    });
}

function initComponent()
{
    $('.list-belanja .row').remove();
}

$(document).ready(function(){
    csrf();
    table = $('#dataTable').DataTable({
        'processing'    : true,
        'serverSide'    : true,

        'ajax' : {
        	'url'	: baseurl + 'belanja/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
                        var button = '<button id="'+ response.data[x].id +'" name="btn_print" class="btn btn-default btn-xs btn-flat" title="Cetak Data"><i class="fa fa-print"></i></button> <button id="'+ response.data[x].id +'" name="btn_download" class="btn btn-success btn-xs btn-flat" title="Download Data"><i class="fa fa-download"></i></button> <button id="'+ response.data[x].id +'" name="btn_terima" class="btn btn-warning btn-xs btn-flat" title="Hasil Belanja"><i class="fa fa-file-text"></i></button>';

	            		row.push({
	            			'no'                : i,
                            'nomor'             : response.data[x].nomor,
                            'total'             : response.data[x].total,
                            'keterangan'        : response.data[x].keterangan,
	            			'aksi'	            : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'total' },
            { 'data' : 'keterangan' },
            { 'data' : 'aksi' }
        ],

        // 'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 4 ]
    		}
  		]
	});
});

$('#dataTable').on('click', 'button[name="btn_terima"]', function(){
    csrf();
    initComponent()
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'belanja/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;
                var daftar_belanja = response.daftar_belanja;

                $('.belanja-detail .col-xs-6:eq(0)').html('NO. PO : <strong>' + d.nomor + '</strong>');
                $('.belanja-detail .col-xs-6:eq(1)').html('NO. INVOICE : <strong>' + d.nomor_invoice + '</strong>');
                $('.belanja-detail .col-xs-6:eq(2)').html('STYLE : <strong>' + d.nama_item + '</strong>');

                if (daftar_belanja.length == 0) {
                    $('.list-belanja').append('<div class="row"><div style="text-align:center;"><i>Daftar belanja belum tersedia. Silakan isi kebutuhan PO terlebih dahulu.</i></div></div>');

                    $('button[name="btn_kembali"]').show();
                    $('button[name="btn_save"]').hide();
                    $('button[name="btn_cancel"]').hide();
                } else{
                    $('button[name="btn_kembali"]').hide();
                    $('button[name="btn_save"]').show();
                    $('button[name="btn_cancel"]').show();
                }

                for (let index = 0; index < daftar_belanja.length; index++) {
                    var row = $('<div class="row">');

                    var fG = $('<div class="form-group col-md-2">');
                    if (index == 0) {
                        fG.append('<label>Bahan Baku</label>');
                    }
                    fG.append('<input type="hidden" name="id_m_bahan_baku[]" value="'+ daftar_belanja[index].id_m_bahan_baku +'" required>');
                    fG.append('<input type="text" class="form-control" value="'+ daftar_belanja[index].nama_bahan_baku +'" readonly>');
                    $(row).append(fG);

                    var fG = $('<div class="form-group col-md-1">');
                    if (index == 0) {
                        fG.append('<label>Qty</label>');
                    }
                    fG.append('<input type="number" name="qty[]" class="form-control" value="'+ daftar_belanja[index].qty +'" placeholder="Qty" required>');
                    $(row).append(fG);

                    var fG = $('<div class="form-group col-md-2">');
                    if (index == 0 ) {
                        fG.append('<label>Satuan</label>');
                    }
                    fG.append('<input type="hidden" name="id_m_satuan[]" value="'+ daftar_belanja[index].id_m_satuan +'" required>');
                    fG.append('<input type="text" class="form-control" value="'+ daftar_belanja[index].nama_satuan +'" readonly>');
                    $(row).append(fG);

                    var fG = $('<div class="form-group col-md-2">');
                    if (index == 0) {
                        fG.append('<label>Harga</label>');
                    }
                    fG.append('<input type="number" name="harga[]" class="form-control" value="'+ daftar_belanja[index].harga +'" placeholder="Harga" required>');
                    $(row).append(fG);

                    var fG = $('<div class="form-group col-md-2">');
                    if (index == 0) {
                        fG.append('<label>Ceklist</label>');
                    }
                    var select = $('<select name="status[]" class="form-control" style="width:100%;">');
                    select.append('<option value="0">Belum</option>');
                    select.append('<option value="1">Sudah</option>');
                    fG.append(select);
                    $(row).append(fG);

                    var fG = $('<div class="form-group col-md-3">');
                    if (index == 0) {
                        fG.append('<label>Keterangan</label>');
                    }
                    var ket = '';
                    if (daftar_belanja[index].keterangan != null) {
                        ket = daftar_belanja[index].keterangan;
                    }
                    fG.append('<textarea name="keterangan_bahan_baku[]" class="form-control" placeholder="Keterangan">'+ ket +'</textarea>');
                    $(row).append(fG);

                    $('.list-belanja').append(row);
                }

                initSelect2();

                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"], button[name="btn_kembali"]').click(function(){
    initComponent();

	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
    $(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'belanja/save/',
        data: {
            'id': $(this).attr('id'),
            'form': $('#formData').serialize(),
            'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
                $.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
                setTimeout(function(){
                    $('#table').fadeIn();
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_print"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'cetak/belanja/' + id);
});

$('#dataTable').on('click', 'button[name="btn_download"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'download/belanja/' + id);
});