var table = '';
var selectBahanBaku = '';
var selectSatuan = '';
var selectStatus = '';

$('.li-po').addClass('menu-open');
$('.li-po .treeview-menu').css('display', 'block');
$('.li-t-kebutuhan').addClass('active');

function csrf()
{
    $.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
                $('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

function select2BahanBaku()
{
    $('select[name="id_m_bahan_baku[]"]').select2({
        tags: true
    });
    $('select[name="id_m_satuan[]"]').select2({
        tags: true
    });
    $('select[name="status[]"]').select2({
        minimumResultsForSearch: Infinity
    });
}

function initComponent()
{
    $('button[name="btn_delete_bahan_baku"]').trigger('click');
    $('select[name="id_m_bahan_baku[]"]').val('0').trigger('change');
    $('input[name="qty[]"]').val('');
    $('input[name="id_m_satuan[]"]').val(0);
    $('input[name="harga[]"]').val('');
    $('input[name="status[]"]').val(0);
    $('textarea[name="keterangan_bahan_baku[]"]').val('');
}

$(document).ready(function(){
    csrf();
    table = $('#dataTable').DataTable({
        'processing'    : true,
        'serverSide'    : true,

        'ajax' : {
        	'url'	: baseurl + 'kebutuhan/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
                        var button = '<button id="'+ response.data[x].id +'" name="btn_print" class="btn btn-default btn-xs btn-flat" title="Cetak Data"><i class="fa fa-print"></i></button> <button id="'+ response.data[x].id +'" name="btn_download" class="btn btn-success btn-xs btn-flat" title="Download Data"><i class="fa fa-download"></i></button> <button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button>';

	            		row.push({
	            			'no'                : i,
                            'nomor'             : response.data[x].nomor,
                            'total'             : response.data[x].total,
                            'keterangan'        : response.data[x].keterangan,
	            			'aksi'	            : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'total' },
            { 'data' : 'keterangan' },
            { 'data' : 'aksi' }
        ],

        // 'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 4 ]
    		}
  		]
	});

    select2BahanBaku();
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
    csrf();
    initComponent()
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'kebutuhan/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;
                var detail = response.detail;
                var bahan_baku = response.bahan_baku;

                $('.kebutuhan-detail .col-xs-6:eq(0)').html('NO. PO : <strong>' + d.nomor + '</strong>');
                $('.kebutuhan-detail .col-xs-6:eq(1)').html('NO. INVOICE : <strong>' + d.nomor_invoice + '</strong>');
                $('.kebutuhan-detail .col-xs-6:eq(2)').html('STYLE : <strong>' + d.nama_item + '</strong>');

                $('#dataTableDetail tbody tr').remove();
                for (let index = 0; index < detail.length; index++) {
                    var tr = $('<tr>');
                    tr.append('<td>'+ detail[index].nama_warna +'</td>');
                    tr.append('<td>'+ detail[index].ukuran_s +'</td>');
                    tr.append('<td>'+ detail[index].ukuran_m +'</td>');
                    tr.append('<td>'+ detail[index].ukuran_l +'</td>');
                    tr.append('<td>'+ detail[index].ukuran_xl +'</td>');
                    tr.append('<td>'+ detail[index].ukuran_xxl +'</td>');
                    tr.append('<td>'+ detail[index].qty +'</td>');
                    $('#dataTableDetail tbody').append(tr);
                }

                for (let index = 0; index < bahan_baku.length; index++) {
                    if (index == 0) {
                        $('select[name="id_m_bahan_baku[]"]:eq(0)').find('option').each(function(){
                            if ($(this).val() == bahan_baku[index].id_m_bahan_baku) {
                                $('select[name="id_m_bahan_baku[]"]:eq(0)').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="qty[]"]:eq(0)').val(bahan_baku[index].qty);
                        $('select[name="id_m_satuan[]"]:eq(0)').find('option').each(function(){
                            if ($(this).val() == bahan_baku[index].id_m_satuan) {
                                $('select[name="id_m_satuan[]"]:eq(0)').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="harga[]"]:eq(0)').val(bahan_baku[index].harga);
                        $('select[name="status[]"]:eq(0)').find('option').each(function(){
                            if ($(this).val() == bahan_baku[index].status) {
                                $('select[name="status[]"]:eq(0)').val($(this).val()).trigger('change');
                            }
                        });
                        $('textarea[name="keterangan_bahan_baku[]"]:eq(0)').val(bahan_baku[index].keterangan);
                    } else{
                        $('button[name="btn_add_bahan_baku"]').trigger('click');
                        $('select[name="id_m_bahan_baku[]"]:eq('+index+')').find('option').each(function(){
                            if ($(this).val() == bahan_baku[index].id_m_bahan_baku) {
                                $('select[name="id_m_bahan_baku[]"]:eq('+index+')').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="qty[]"]:eq('+index+')').val(bahan_baku[index].qty);
                        $('select[name="id_m_satuan[]"]:eq('+index+')').find('option').each(function(){
                            if ($(this).val() == bahan_baku[index].id_m_satuan) {
                                $('select[name="id_m_satuan[]"]:eq('+index+')').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="harga[]"]:eq('+index+')').val(bahan_baku[index].harga);
                        $('select[name="status[]"]:eq('+index+')').find('option').each(function(){
                            if ($(this).val() == bahan_baku[index].status) {
                                $('select[name="status[]"]:eq('+index+')').val($(this).val()).trigger('change');
                            }
                        });
                        $('textarea[name="keterangan_bahan_baku[]"]:eq('+index+')').val(bahan_baku[index].keterangan);
                    }
                    
                }

                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
    initComponent();

	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
    $(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    $('#formData').find('select[name="id_m_bahan_baku[]"]').each(function(){
        if($(this).val() == 0){
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: 'Silakan pilih bahan baku terlebih dahulu.'
            }, {
                type: 'warning',
                delay: 1000,
                timer: 500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
            $(this).focus();
            missing = true;
            return false;
        }
    });

    $('#formData').find('select[name="id_m_satuan[]"]').each(function(){
        if($(this).val() == 0){
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: 'Silakan pilih satuan terlebih dahulu.'
            }, {
                type: 'warning',
                delay: 1000,
                timer: 500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
            $(this).focus();
            missing = true;
            return false;
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'kebutuhan/save/',
        data: {
            'id': $(this).attr('id'),
            'form': $('#formData').serialize(),
            'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
                $.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
                setTimeout(function(){
                    $('#table').fadeIn();
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$(document).on('click', 'button[name="btn_add_bahan_baku"]', function(e){
    e.preventDefault();
    var template = $('template[name="bahan_baku"]').html();
    $('.list-bahan-baku').append(template);
    select2BahanBaku();
});

$(document).on('click', 'button[name="btn_delete_bahan_baku"]', function(e){
    e.preventDefault();
    $(this).closest('.row').remove();
});

$('#dataTable').on('click', 'button[name="btn_print"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'cetak/kebutuhan/' + id);
});

$('#dataTable').on('click', 'button[name="btn_download"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'download/kebutuhan/' + id);
});