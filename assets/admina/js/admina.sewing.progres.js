var table = '';
var selectPenjahit = '';

$('.li-sewing').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

function initComponent()
{
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $('input[name="ukuran_s[]"]').trigger('keyup');
}

$(document).ready(function(){
    csrf();
    initComponent();
});

$('button[name="btn_cancel"]').click(function(){
    window.location.href = baseurl + 'sewing/';
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    if(missing){
        $(this).removeAttr('disabled');
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'sewing/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            csrf();
            $(this).removeAttr('disabled');
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				setTimeout(function(){
					window.location.href = baseurl + 'sewing/'
				}, 1000);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$(document).on('click', 'button[name="btn_add_progres"]', function(){
    var template = $(this).closest('.baris').html();
    template = $(template).first('.row');
    template.each(function(){
        $(this).find('input[type="number"]').val('');
        $(this).find('input[name="tanggal[]"]').val('');
        $(this).find('input[name="progres_id[]"]').val(0);
        $(this).find('button').attr('name', 'btn_delete_progres');
        $(this).find('button').addClass('btn-danger').removeClass('btn-success');
        $(this).find('button').find('i').addClass('fa-trash').removeClass('fa-plus');
    });
    $(this).closest('.baris').append(template);
    initComponent();
});

$(document).on('click', 'button[name="btn_delete_progres"]', function(){
    $(this).closest('.row').remove();
    initComponent();
});

$(document).on('keyup', 'input[name^="ukuran_"]', function(){
    var s = parseInt($(this).closest('.row').find('input[name="ukuran_s[]"]').val()) || 0;
    var m = parseInt($(this).closest('.row').find('input[name="ukuran_m[]"]').val()) || 0;
    var l = parseInt($(this).closest('.row').find('input[name="ukuran_l[]"]').val()) || 0;
    var xl = parseInt($(this).closest('.row').find('input[name="ukuran_xl[]"]').val()) || 0;
    var xxl = parseInt($(this).closest('.row').find('input[name="ukuran_xxl[]"]').val()) || 0;
    var qty = s + m + l + xl + xxl;
    $(this).closest('.row').find('input[name="qty[]"]').val(qty);

    var totalS = 0;
    var totalM = 0;
    var totalL = 0;
    var totalXL = 0;
    var totalXXL = 0;
    $(this).closest('.baris').each(function(){
        $(this).find('.row').each(function(){
            totalS = totalS + (parseInt($(this).find('input[name="ukuran_s[]"]').val()) || 0);
            totalM = totalM + (parseInt($(this).find('input[name="ukuran_m[]"]').val()) || 0);
            totalL = totalL + (parseInt($(this).find('input[name="ukuran_l[]"]').val()) || 0);
            totalXL = totalXL + (parseInt($(this).find('input[name="ukuran_xl[]"]').val()) || 0);
            totalXXL = totalXXL + (parseInt($(this).find('input[name="ukuran_xxl[]"]').val()) || 0);
        });
    });
    var totalQty = totalS + totalM + totalL + totalXL + totalXXL;
    $(this).closest('.baris').find('input[name="balance[]"]').val('');
    $(this).closest('.baris').find('input[name="balance[]"]:last').val(totalQty);

    var qtyWarna = parseInt($(this).closest('tr').find('input[name="qty_warna"]').val()) || 0;
    $(this).closest('.baris').find('input[name="qty_balance[]"]').val('');
    $(this).closest('.baris').find('input[name="qty_balance[]"]:last').val(qtyWarna - totalQty);
});