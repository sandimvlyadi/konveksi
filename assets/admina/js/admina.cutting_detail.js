var table = '';
var selectNomorpo = '';
var selectPenjahit = '';

$('.li-po').addClass('menu-open');
$('.li-po .treeview-menu').css('display', 'block');
$('.li-t-cutting_detail').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

$(document).ready(function(){
	csrf();
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'cutting_detail/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
            			var button = '<button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button> <button id="'+ response.data[x].id +'" name="btn_delete" class="btn btn-danger btn-xs btn-flat" title="Hapus Data"><i class="fa fa-trash"></i></button>';

	            		row.push({
	            			'no'            : i,
                            'nomor'         : response.data[x].nomor,
                            'nama_penjahit' : response.data[x].nama_penjahit,
                            'ukuran_s'      : response.data[x].ukuran_s,
                            'ukuran_m'      : response.data[x].ukuran_m,
                            'ukuran_l'      : response.data[x].ukuran_l,
                            'keterangan'    : response.data[x].keterangan,
	            			'aksi'	        : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'nama_penjahit' },
            { 'data' : 'ukuran_s' },
            { 'data' : 'ukuran_m' },
            { 'data' : 'ukuran_l' },
            { 'data' : 'keterangan' },
        	{ 'data' : 'aksi' }
        ],

        'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 7 ]
    		}
  		]
	});

    $.ajax({
        type: 'GET',
        url: baseurl + 'po/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_t_po"]').append('<option value="0">- Pilih Nomor PO -</option>');
                for(var x in response.data){
                    $('select[name="id_t_po"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nomor+'</option>');
                }
            } else{
                $('select[name="id_t_po"]').append('<option value="0">- Pilih Nomor PO -</option>');
            }
        }
    });
    selectNomorpo = $('select[name="id_t_po"]').select2();

    $.ajax({
        type: 'GET',
        url: baseurl + 'master_penjahit/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_m_penjahit"]').append('<option value="0">- Pilih Penjahit -</option>');
                for(var x in response.data){
                    $('select[name="id_m_penjahit"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nama_penjahit+'</option>');
                }
            } else{
                $('select[name="id_m_penjahit"]').append('<option value="0">- Pilih Penjahit -</option>');
            }
        }
    });
    selectPenjahit = $('select[name="id_m_penjahit"]').select2();
    
});

$('button[name="btn_add"]').click(function(){
	csrf();
	$('button[name="btn_save"]').attr('id', '0');
    $(selectNomorpo).val('0').trigger('change');
    $(selectPenjahit).val('0').trigger('change');
    $('input[name="ukuran_s"]').val('0');
    $('input[name="ukuran_m"]').val('0');
    $('input[name="ukuran_l"]').val('0');
    $('textarea[name="keterangan"]').val('');

    $('#formTitle').text('Tambah Data');

	$('#table').hide();
	setTimeout(function(){
		$('#form').fadeIn()
	}, 100);
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
	csrf();
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'cutting_detail/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;

                $(selectNomorpo).find('option').each(function(){
                    if ($(this).val() == d.id_t_po) {
                        $(selectNomorpo).val($(this).val()).trigger('change');
                    }
                });
                $(selectPenjahit).find('option').each(function(){
                    if ($(this).val() == d.id_m_penjahit) {
                        $(selectPenjahit).val($(this).val()).trigger('change');
                    }
                });
                $('input[name="ukuran_s"]').val(d.ukuran_s);
                $('input[name="ukuran_m"]').val(d.ukuran_m);
                $('input[name="ukuran_l"]').val(d.ukuran_l);
                $('textarea[name="keterangan"]').val(d.keterangan);
                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_delete"]', function(){
	if (!confirm('Apakah anda yakin?')) {
		return;
	}

	var id = $(this).attr('id');

	$.ajax({
        type: 'POST',
        url: baseurl + 'cutting_detail/delete/',
        data: {
        	'id': id,
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                table.ajax.reload(null, false);
				csrf();
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    if ($('select[name="id_t_po"]').val() == 0) {
        $.notify({
            icon: 'glyphicon glyphicon-info-sign',
            message: 'Silakan pilih nomor po terlebih dahulu.'
        }, {
            type: 'warning',
            delay: 1000,
            timer: 500,
            placement: {
              from: 'top',
              align: 'center'
            }
        });
        $(this).focus();
        return;
    }

    if ($('select[name="id_m_penjahit"]').val() == 0) {
        $.notify({
            icon: 'glyphicon glyphicon-info-sign',
            message: 'Silakan pilih penjahit terlebih dahulu.'
        }, {
            type: 'warning',
            delay: 1000,
            timer: 500,
            placement: {
              from: 'top',
              align: 'center'
            }
        });
        $(this).focus();
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'cutting_detail/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
				setTimeout(function(){
					$('#table').fadeIn();
				}, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('select[name="id_t_po"]').on('change', function(){
    var id = $(this).val();

    $('select[name="id_m_penjahit"]').empty();
    $('select[name="id_m_penjahit"]').append('<option value="0">Pilih Penjahit</option>');
    $.ajax({
        type: 'GET',
        url: baseurl + 'cutting/select-by-cut/' + id,
        dataType: 'json',
        success: function(response){
            if(response.result){
                for(var x in response.data){
                    $('select[name="id_t_po"]').append('<option value="'+ response.data[x].id_m_penjahit +'">'+response.data[x].nama_penjahit+'</option>');
                }
            }
        }
    });
});