var table = '';
// var selectPayment = '';
var selectCustomer = '';
var selectWarna = '';

$('.li-po').addClass('menu-open');
$('.li-po .treeview-menu').css('display', 'block');
$('.li-t-po').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

function select2Warna()
{
    $('select[name="id_m_warna[]"]').select2({
        tags: true
    });
}

function initInputMask()
{
    $('.inputmask').inputmask('decimal', {
        radixPoint:",",
        groupSeparator: ".",
        autoGroup: true,
        digits: 0,
        digitsOptional: false,
        placeholder: '0',
        rightAlign: false,
        prefix: 'Rp. ',
        onBeforeMask: function (value, opts) {
            return value;
        },
    });   
}

function initCustomer()
{
    $('input[name="nama_customer"]').val('');
    $('input[name="alamat_customer"]').val('');
    $('input[name="kontak_customer"]').val('');
    $('textarea[name="keterangan_customer"]').val('');
}

function initComponent()
{
    // $(selectPayment).val('0').trigger('change');
    $(selectCustomer).val('0').trigger('change');
    $('input[name="nomor"]').val('');
    $('input[name="tanggal"]').val('');
    $('input[name="nama_item"]').val('');
    $('input[name="dp"]').val('0');
    $('input[name="total_qty"]').val('0');
    $('input[name="total_pembayaran"]').val('0');
    $('textarea[name="keterangan"]').val('');
    $('button[name="btn_delete_warna"]').trigger('click');
    $('select[name="id_m_warna[]"]').val('0').trigger('change');
    $('input[name="ukuran_s[]"]').val('0');
    $('input[name="ukuran_m[]"]').val('0');
    $('input[name="ukuran_l[]"]').val('0');
    $('input[name="ukuran_xl[]"]').val('0');
    $('input[name="ukuran_xxl[]"]').val('0');
    $('input[name="qty[]"]').val('0');
    $('input[name="harga[]"]').val('0');
    $('input[name="total[]"]').val('0');
    $('textarea[name="keterangan_warna[]"]').val('');
}

$(document).ready(function(){
	csrf();
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'purchase-order/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
                        var button = '<button id="'+ response.data[x].id +'" name="btn_print" class="btn btn-default btn-xs btn-flat" title="Cetak Data"><i class="fa fa-print"></i></button> <button id="'+ response.data[x].id +'" name="btn_download" class="btn btn-success btn-xs btn-flat" title="Download Data"><i class="fa fa-download"></i></button> <button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button> <button id="'+ response.data[x].id +'" name="btn_delete" class="btn btn-danger btn-xs btn-flat" title="Hapus Data"><i class="fa fa-trash"></i></button>';

	            		row.push({
	            			'no'                : i,
                            'nomor'             : response.data[x].nomor,
                            'tanggal'           : response.data[x].tanggal,
                            'nama_customer'     : response.data[x].nama_customer,
                            'nama_item'         : response.data[x].nama_item,
                            'keterangan'        : response.data[x].keterangan,
	            			'aksi'              : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'tanggal' },
            { 'data' : 'nama_customer' },
            { 'data' : 'nama_item' },
            { 'data' : 'keterangan' },
        	{ 'data' : 'aksi' }
        ],

        // 'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 6 ]
    		}
  		]
	});

    $.ajax({
        type: 'GET',
        url: baseurl + 'master-customer/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_m_customer"]').append('<option value="0">- Pilih Customer -</option>');
                for(var x in response.data){
                    $('select[name="id_m_customer"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nama_customer+'</option>');
                }
            } else{
                $('select[name="id_m_customer"]').append('<option value="0">- Pilih Customer -</option>');
            }
        }
    });
    selectCustomer = $('select[name="id_m_customer"]').select2({
        tags: true
    });

    // $.ajax({
    //     type: 'GET',
    //     url: baseurl + 'master-payment/select/',
    //     dataType: 'json',
    //     success: function(response){
    //         if(response.result){
    //             $('select[name="id_m_payment"]').append('<option value="0">- Pilih Metode Pembayaran -</option>');
    //             for(var x in response.data){
    //                 $('select[name="id_m_payment"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nama_payment+'</option>');
    //             }
    //         } else{
    //             $('select[name="id_m_payment"]').append('<option value="0">- Pilih Metode Pembayaran -</option>');
    //         }
    //     }
    // });
    // selectPayment = $('select[name="id_m_payment"]').select2();

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    select2Warna();
    initInputMask();
});

$('button[name="btn_add"]').click(function(){
    csrf();
    initComponent()
	$('button[name="btn_save"]').attr('id', '0');

    $('#formTitle').text('Tambah Data');

	$('#table').hide();
	setTimeout(function(){
		$('#form').fadeIn()
	}, 100);
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
    csrf();
    initComponent()
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'purchase-order/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;
                var warna = response.warna;

                $(selectCustomer).find('option').each(function(){
                    if ($(this).val() == d.id_m_customer) {
                        $(selectCustomer).val($(this).val()).trigger('change');
                    }
                });
                // $(selectPayment).find('option').each(function(){
                //     if ($(this).val() == d.id_m_payment) {
                //         $(selectPayment).val($(this).val()).trigger('change');
                //     }
                // });
                $('input[name="nomor"]').val(d.nomor);
                $('input[name="nomor_invoice"]').val(d.nomor_invoice);
                $('input[name="tanggal"]').val(d.tanggal);
                $('input[name="nama_item"]').val(d.nama_item);
                $('input[name="dp"]').val(d.dp);
                $('textarea[name="keterangan"]').val(d.keterangan);
                for (let index = 0; index < warna.length; index++) {
                    if (index == 0) {
                        $('select[name="id_m_warna[]"]:eq(0)').find('option').each(function(){
                            if ($(this).val() == warna[index].id_m_warna) {
                                $('select[name="id_m_warna[]"]:eq(0)').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="ukuran_s[]"]:eq(0)').val(warna[index].ukuran_s);
                        $('input[name="ukuran_m[]"]:eq(0)').val(warna[index].ukuran_m);
                        $('input[name="ukuran_l[]"]:eq(0)').val(warna[index].ukuran_l);
                        $('input[name="ukuran_xl[]"]:eq(0)').val(warna[index].ukuran_xl);
                        $('input[name="ukuran_xxl[]"]:eq(0)').val(warna[index].ukuran_xxl);
                        $('input[name="qty[]"]:eq(0)').val(warna[index].qty);
                        $('input[name="harga[]"]:eq(0)').val(warna[index].harga);
                        $('input[name="total[]"]:eq(0)').val(warna[index].total);
                        $('textarea[name="keterangan_warna[]"]:eq(0)').val(warna[index].keterangan);
                    } else{
                        $('button[name="btn_add_warna"]').trigger('click');
                        $('select[name="id_m_warna[]"]:eq('+index+')').find('option').each(function(){
                            if ($(this).val() == warna[index].id_m_warna) {
                                $('select[name="id_m_warna[]"]:eq('+index+')').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="ukuran_s[]"]:eq('+index+')').val(warna[index].ukuran_s);
                        $('input[name="ukuran_m[]"]:eq('+index+')').val(warna[index].ukuran_m);
                        $('input[name="ukuran_l[]"]:eq('+index+')').val(warna[index].ukuran_l);
                        $('input[name="ukuran_xl[]"]:eq('+index+')').val(warna[index].ukuran_xl);
                        $('input[name="ukuran_xxl[]"]:eq('+index+')').val(warna[index].ukuran_xxl);
                        $('input[name="qty[]"]:eq('+index+')').val(warna[index].qty);
                        $('input[name="harga[]"]:eq('+index+')').val(warna[index].harga);
                        $('input[name="total[]"]:eq('+index+')').val(warna[index].total);
                        $('textarea[name="keterangan_warna[]"]:eq('+index+')').val(warna[index].keterangan);
                    }
                    
                }
                summarize();

                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_delete"]', function(){
	if (!confirm('Apakah anda yakin?')) {
		return;
	}

	var id = $(this).attr('id');

	$.ajax({
        type: 'POST',
        url: baseurl + 'purchase-order/delete/',
        data: {
        	'id': id,
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                table.ajax.reload(null, false);
				csrf();
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
    initComponent();

	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    if (missing) {
        $('button[name="btn_save"]').removeAttr('disabled');
        return;
    }

    if ($('input[name="nama_customer"]').val() != '') {
        if ($('input[name="kontak_customer"]').val() == '') {
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: 'Silakan input kontak customer baru terlebih dahulu.'
            }, {
                type: 'warning',
                delay: 1000,
                timer: 500,
                placement: {
                  from: 'top',
                  align: 'center'
                }
            });
            $('input[name="kontak_customer"]').focus();
            $('button[name="btn_save"]').removeAttr('disabled');
            return;
        }
    }

    if ($('select[name="id_m_customer"]').val() == 0) {
        $.notify({
            icon: 'glyphicon glyphicon-info-sign',
            message: 'Silakan pilih customer terlebih dahulu.'
        }, {
            type: 'warning',
            delay: 1000,
            timer: 500,
            placement: {
              from: 'top',
              align: 'center'
            }
        });
        $('select[name="id_m_customer"]').select2('open');
        $('button[name="btn_save"]').removeAttr('disabled');
        return;
    }

    // if ($('select[name="id_m_payment"]').val() == 0) {
    //     $.notify({
    //         icon: 'glyphicon glyphicon-info-sign',
    //         message: 'Silakan pilih metode pembayaran terlebih dahulu.'
    //     }, {
    //         type: 'warning',
    //         delay: 1000,
    //         timer: 500,
    //         placement: {
    //           from: 'top',
    //           align: 'center'
    //         }
    //     });
    //     $('select[name="id_m_payment"]').select2('open');
    //     $('button[name="btn_save"]').removeAttr('disabled');
    //     return;
    // }

    var index = 0;
    $('#formData').find('select[name="id_m_warna[]"]').each(function(){
        if($(this).val() == 0){
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: 'Silakan pilih warna terlebih dahulu.'
            }, {
                type: 'warning',
                delay: 1000,
                timer: 500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
            $(this).select2('open');
            missing = true;
            return false;
        }
        index = index + 1;
    });

    if (missing) {
        $('button[name="btn_save"]').removeAttr('disabled');
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'purchase-order/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
				setTimeout(function(){
					$('#table').fadeIn();
				}, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$(document).on('click', 'button[name="btn_add_warna"]', function(e){
    e.preventDefault();
    var template = $('template[name="warna"]').html();
    $('.list-warna').append(template);
    select2Warna();
    initInputMask();
    summarize();
});

$(document).on('click', 'button[name="btn_delete_warna"]', function(e){
    e.preventDefault();
    $(this).closest('.row').remove();
    summarize();
});

$(document).on('change', 'input[name^="ukuran"], input[name="harga[]"]', function(){
    var s = parseInt($(this).closest('.baris').find('input[name="ukuran_s[]"]').val());
    var m = parseInt($(this).closest('.baris').find('input[name="ukuran_m[]"]').val());
    var l = parseInt($(this).closest('.baris').find('input[name="ukuran_l[]"]').val());
    var xl = parseInt($(this).closest('.baris').find('input[name="ukuran_xl[]"]').val());
    var xxl = parseInt($(this).closest('.baris').find('input[name="ukuran_xxl[]"]').val());
    var harga = $(this).closest('.baris').find('input[name="harga[]"]').val();
    harga = harga.replace('Rp. ', '');
    harga = harga.replace(/\./g, '');
    harga = parseInt(harga);
    var qty = s+m+l+xl+xxl;
    var total = qty*harga;
    $(this).closest('.baris').find('input[name="qty[]"]').val(qty);
    $(this).closest('.baris').find('input[name="total[]"]').val(total);
    summarize();
});

function summarize()
{
    var totalQty = 0;
    $('input[name="qty[]"]').each(function(){
        totalQty = totalQty + parseInt($(this).val());
    });
    $('input[name="total_qty"]').val(totalQty);

    var totalPembayaran = 0;
    $('input[name="total[]"]').each(function(){
        var pembayaran = $(this).val();
        pembayaran = pembayaran.replace('Rp. ', '');
        pembayaran = pembayaran.replace(/\./g, '');
        totalPembayaran = totalPembayaran + parseInt(pembayaran);
    });
    $('input[name="total_pembayaran"]').val(totalPembayaran);
}

$(document).on('change', 'select[name="id_m_customer"]', function(){
    var nama = $(this).val();
    initCustomer();
    if (isNaN(parseInt(nama))) {
        $('input[name="nama_customer"]').val(nama);
        $('.customer').fadeIn();
    } else{
        $('.customer').fadeOut();
    }
});

$('#dataTable').on('click', 'button[name="btn_print"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'cetak/purchase-order/' + id);
});

$('#dataTable').on('click', 'button[name="btn_download"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'download/purchase-order/' + id);
});