var table = '';
var selectPo = '';
var selectBiaya = '';

$('.li-delivery').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

function select2Biaya()
{
    $('select[name="id_m_biaya[]"]').select2()
}

function initComponent()
{
    $(selectPo).val('0').trigger('change');
    $('input[name="target"]').val('');
    $('textarea[name="keterangan"]').val('');
    $('button[name="btn_delete_biaya"]').trigger('click');
    $('select[name="id_m_biaya[]"]').val('0').trigger('change');
    $('input[name="biaya_delivery[]"]').val('');
    $('textarea[name="keterangan_biaya[]"]').val('');
}

$(document).ready(function(){
	csrf();
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'delivery/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
                        var button = '<button id="'+ response.data[x].id +'" name="btn_progres" class="btn btn-warning btn-xs btn-flat" title="Tambah Progres"><i class="fa fa-file-text"></i></button> <button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button> <button id="'+ response.data[x].id +'" name="btn_delete" class="btn btn-danger btn-xs btn-flat" title="Hapus Data"><i class="fa fa-trash"></i></button>';
                        
                        var jumlah_selesai = parseInt(response.data[x].ukuran_s) + parseInt(response.data[x].ukuran_m) + parseInt(response.data[x].ukuran_l) + parseInt(response.data[x].ukuran_xl) + parseInt(response.data[x].ukuran_xxl);

	            		row.push({
	            			'no'                : i,
                            'nomor'             : response.data[x].nomor,
                            'jumlah'            : response.data[x].jumlah,
                            'jumlah_selesai'    : jumlah_selesai ? jumlah_selesai : 0,
                            'target'            : response.data[x].target,
                            'keterangan'        : response.data[x].keterangan,
	            			'aksi'	            : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'jumlah' },
            { 'data' : 'jumlah_selesai' },
            { 'data' : 'target' },
            { 'data' : 'keterangan' },
        	{ 'data' : 'aksi' }
        ],

        // 'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 6 ]
    		}
  		]
	});

    $.ajax({
        type: 'GET',
        url: baseurl + 'purchase-order/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_t_po"]').append('<option value="0">- Pilih Nomor PO -</option>');
                for(var x in response.data){
                    $('select[name="id_t_po"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nomor+'</option>');
                }
            } else{
                $('select[name="id_t_po"]').append('<option value="0">- Pilih Nomor PO -</option>');
            }
        }
    });
    selectPo = $('select[name="id_t_po"]').select2();

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    select2Biaya();
});

$('button[name="btn_add"]').click(function(){
    csrf();
    initComponent()
	$('button[name="btn_save"]').attr('id', '0');

    $('#formTitle').text('Tambah Data');

	$('#table').hide();
	setTimeout(function(){
		$('#form').fadeIn()
	}, 100);
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
    csrf();
    initComponent()
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'delivery/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;
                var biaya = response.biaya;

                $(selectPo).find('option').each(function(){
                    if ($(this).val() == d.id_t_po) {
                        $(selectPo).val($(this).val()).trigger('change');
                    }
                });
                $('input[name="target"]').val(d.target);
                $('textarea[name="keterangan"]').val(d.keterangan);
                for (let index = 0; index < biaya.length; index++) {
                    if (index == 0) {
                        $('select[name="id_m_biaya[]"]:eq(0)').find('option').each(function(){
                            if ($(this).val() == biaya[index].id_m_biaya) {
                                $('select[name="id_m_biaya[]"]:eq(0)').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="biaya_delivery[]"]:eq(0)').val(biaya[index].biaya_delivery);
                        $('textarea[name="keterangan_biaya[]"]:eq(0)').val(biaya[index].keterangan);
                    } else{
                        $('button[name="btn_add_biaya"]').trigger('click');
                        $('select[name="id_m_biaya[]"]:eq('+index+')').find('option').each(function(){
                            if ($(this).val() == biaya[index].id_m_biaya) {
                                $('select[name="id_m_biaya[]"]:eq('+index+')').val($(this).val()).trigger('change');
                            }
                        });
                        $('input[name="biaya_delivery[]"]:eq('+index+')').val(biaya[index].biaya_delivery);
                        $('textarea[name="keterangan_biaya[]"]:eq('+index+')').val(biaya[index].keterangan);
                    }
                    
                }

                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_delete"]', function(){
	if (!confirm('Apakah anda yakin?')) {
		return;
	}

	var id = $(this).attr('id');

	$.ajax({
        type: 'POST',
        url: baseurl + 'delivery/delete/',
        data: {
        	'id': id,
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                table.ajax.reload(null, false);
				csrf();
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
    initComponent();

	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    if ($('select[name="id_t_po"]').val() == 0) {
        $.notify({
            icon: 'glyphicon glyphicon-info-sign',
            message: 'Silakan pilih nomor po terlebih dahulu.'
        }, {
            type: 'warning',
            delay: 1000,
            timer: 500,
            placement: {
              from: 'top',
              align: 'center'
            }
        });
        $(this).focus();
        return;
    }

    $('#formData').find('select[name="id_m_biaya[]"]').each(function(){
        if($(this).val() == 0){
            $.notify({
                icon: 'glyphicon glyphicon-info-sign',
                message: 'Silakan pilih biaya terlebih dahulu.'
            }, {
                type: 'warning',
                delay: 1000,
                timer: 500,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
            $(this).focus();
            missing = true;
            return false;
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'delivery/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
				setTimeout(function(){
					$('#table').fadeIn();
				}, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$(document).on('click', 'button[name="btn_add_biaya"]', function(e){
    e.preventDefault();
    var template = $('template[name="biaya"]').html();
    $('.list-biaya').append(template);
    select2Biaya();
});

$(document).on('click', 'button[name="btn_delete_biaya"]', function(e){
    e.preventDefault();
    $(this).closest('.row').remove();
});

$('#dataTable').on('click', 'button[name="btn_progres"]', function(){
    var id = $(this).attr('id');
    window.location.href = baseurl + 'delivery/progres/' + id;
});