var table = '';
var selectPo = '';
var selectCustomer = '';

$('.li-po').addClass('menu-open');
$('.li-po .treeview-menu').css('display', 'block');
$('.li-t-po').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

$(document).ready(function(){
	csrf();
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'po/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
            			var button = '<button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button> <button id="'+ response.data[x].id +'" name="btn_delete" class="btn btn-danger btn-xs btn-flat" title="Hapus Data"><i class="fa fa-trash"></i></button>';

	            		row.push({
	            			'no'            : i,
                            'nomor'         : response.data[x].nomor,
                            'nama_customer' : response.data[x].nama_customer,
                            'tanggal'       : response.data[x].tanggal,
                            'jatuh_tempo'   : response.data[x].jatuh_tempo,
                            'nama_po'       : response.data[x].nama_po,
                            'jumlah'        : response.data[x].jumlah,
                            'total'         : response.data[x].total,
                            'dp'            : response.data[x].dp,
                            'pelunasan'     : response.data[x].pelunasan,
                            'keterangan'    : response.data[x].keterangan,
	            			'aksi'	        : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'nama_customer' },
            { 'data' : 'tanggal' },
            { 'data' : 'jatuh_tempo' },
            { 'data' : 'nama_po' },
            { 'data' : 'jumlah' },
            { 'data' : 'total' },
            { 'data' : 'dp' },
            { 'data' : 'pelunasan' },
            { 'data' : 'keterangan' },
        	{ 'data' : 'aksi' }
        ],

        'order' 	: [[ 3, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 11 ]
    		}
  		]
	});

    $.ajax({
        type: 'GET',
        url: baseurl + 'master-po/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_m_po"]').append('<option value="0">- Pilih Nama PO -</option>');
                for(var x in response.data){
                    $('select[name="id_m_po"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nama_po+'</option>');
                }
            } else{
                $('select[name="id_m_po"]').append('<option value="0">- Pilih Nama PO -</option>');
            }
        }
    });
    selectPo = $('select[name="id_m_po"]').select2();
    
    $.ajax({
        type: 'GET',
        url: baseurl + 'master-customer/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_m_customer"]').append('<option value="0">- Pilih Customer -</option>');
                for(var x in response.data){
                    $('select[name="id_m_customer"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].nama_customer+'</option>');
                }
            } else{
                $('select[name="id_m_customer"]').append('<option value="0">- Pilih Customer -</option>');
            }
        }
    });
    selectCustomer = $('select[name="id_m_customer"]').select2();

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
});

$('button[name="btn_add"]').click(function(){
	csrf();
	$('button[name="btn_save"]').attr('id', '0');
    $('input[name="nomor"]').val('');
    $('input[name="tanggal"]').val('');
    $('input[name="jatuh_tempo"]').val('');
    $(selectPo).val('0').trigger('change');
    $(selectCustomer).val('0').trigger('change');
    $('input[name="ukuran_s"]').val('0');
    $('input[name="ukuran_m"]').val('0');
    $('input[name="ukuran_l"]').val('0');
    $('input[name="ukuran_xl"]').val('0');
    $('input[name="ukuran_xxl"]').val('0');
    $('input[name="jumlah"]').val('0');
    $('input[name="harga"]').val('0');
    $('input[name="total"]').val('0');
    $('input[name="dp"]').val('0');
    $('input[name="pelunasan"]').val('0');
    $('textarea[name="keterangan"]').val('');

    $('#formTitle').text('Tambah Data');

	$('#table').hide();
	setTimeout(function(){
		$('#form').fadeIn()
	}, 100);
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
	csrf();
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'po/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;

                $('input[name="nomor"]').val(d.nomor);
                $('input[name="tanggal"]').val(d.tanggal);
                $('input[name="jatuh_tempo"]').val(d.jatuh_tempo);
                $(selectPo).find('option').each(function(){
                    if ($(this).val() == d.id_m_po) {
                        $(selectPo).val($(this).val()).trigger('change');
                    }
                });
                $(selectCustomer).find('option').each(function(){
                    if ($(this).val() == d.id_m_customer) {
                        $(selectCustomer).val($(this).val()).trigger('change');
                    }
                });
                $('input[name="ukuran_s"]').val(d.ukuran_s);
                $('input[name="ukuran_m"]').val(d.ukuran_m);
                $('input[name="ukuran_l"]').val(d.ukuran_l);
                $('input[name="ukuran_xl"]').val(d.ukuran_xl);
                $('input[name="ukuran_xxl"]').val(d.ukuran_xxl);
                $('input[name="jumlah"]').val(d.jumlah);
                $('input[name="harga"]').val(d.harga);
                $('input[name="total"]').val(d.total);
                $('input[name="dp"]').val(d.dp);
                $('input[name="pelunasan"]').val(d.pelunasan);
                $('textarea[name="keterangan"]').val(d.keterangan);

                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_delete"]', function(){
	if (!confirm('Apakah anda yakin?')) {
		return;
	}

	var id = $(this).attr('id');

	$.ajax({
        type: 'POST',
        url: baseurl + 'po/delete/',
        data: {
        	'id': id,
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                table.ajax.reload(null, false);
				csrf();
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    if ($('select[name="id_m_po"]').val() == 0) {
        $.notify({
            icon: 'glyphicon glyphicon-info-sign',
            message: 'Silakan pilih nama po terlebih dahulu.'
        }, {
            type: 'warning',
            delay: 1000,
            timer: 500,
            placement: {
              from: 'top',
              align: 'center'
            }
        });
        $(this).focus();
        return;
    }

    if ($('select[name="id_m_customer"]').val() == 0) {
        $.notify({
            icon: 'glyphicon glyphicon-info-sign',
            message: 'Silakan pilih customer terlebih dahulu.'
        }, {
            type: 'warning',
            delay: 1000,
            timer: 500,
            placement: {
              from: 'top',
              align: 'center'
            }
        });
        $(this).focus();
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'po/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
				setTimeout(function(){
					$('#table').fadeIn();
				}, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('input[name="ukuran_s"], input[name="ukuran_m"], input[name="ukuran_l"], input[name="ukuran_xl"], input[name="ukuran_xxl"]').on('keyup', function(){
    var s = parseInt($('input[name="ukuran_s"]').val());
    var m = parseInt($('input[name="ukuran_m"]').val());
    var l = parseInt($('input[name="ukuran_l"]').val());
    var xl = parseInt($('input[name="ukuran_xl"]').val());
    var xxl = parseInt($('input[name="ukuran_xxl"]').val());
    $('input[name="jumlah"]').val(s+m+l+xl+xxl);
});

$('input[name="harga"]').on('keyup', function(){
    var harga = parseInt($(this).val());
    var jumlah = parseInt($('input[name="jumlah"]').val());
    $('input[name="total"]').val(harga*jumlah);
});

$('input[name="dp"]').on('keyup', function(){
    var dp = parseInt($(this).val());
    var total = parseInt($('input[name="total"]').val());
    $('input[name="pelunasan"]').val(total-dp);
});