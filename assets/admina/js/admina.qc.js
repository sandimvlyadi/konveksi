var table = '';

$('.li-qc').addClass('active');

$(document).ready(function(){
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'qc/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
                        var button = '<button id="'+ response.data[x].id +'" name="btn_print" class="btn btn-default btn-xs btn-flat" title="Cetak Data"><i class="fa fa-print"></i></button> <button id="'+ response.data[x].id +'" name="btn_download" class="btn btn-success btn-xs btn-flat" title="Download Data"><i class="fa fa-download"></i></button> <button id="'+ response.data[x].id +'" name="btn_progres" class="btn btn-warning btn-xs btn-flat" title="Tambah Progres"><i class="fa fa-file-text"></i></button>';

	            		row.push({
	            			'no'                : i,
                            'nomor'             : response.data[x].nomor,
                            'jumlah'            : response.data[x].jumlah,
                            'jumlah_selesai'    : response.data[x].jumlah_selesai,
                            'keterangan'        : response.data[x].keterangan,
	            			'aksi'	            : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'jumlah' },
            { 'data' : 'jumlah_selesai' },
            { 'data' : 'keterangan' },
        	{ 'data' : 'aksi' }
        ],

        // 'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 5 ]
    		}
  		]
	});
});

$('#dataTable').on('click', 'button[name="btn_progres"]', function(){
    var id = $(this).attr('id');
    window.location.href = baseurl + 'qc/progres/' + id;
});

$('#dataTable').on('click', 'button[name="btn_print"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'cetak/qc/' + id);
});

$('#dataTable').on('click', 'button[name="btn_download"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'download/qc/' + id);
});