var table = '';

$('.li-surat-jalan').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

function initComponent()
{
    $('input[name="nomor"]').val('');
    $('input[name="tanggal"]').val('');
    $('input[name="jumlah_koli"]').val('');
    $('input[name="harga"]').val('');
    $('textarea[name="keterangan"]').val('');
    $('button[name="btn_delete_detail"]').trigger('click');
    $('input[name="no_order[]"]').val('');
    $('input[name="nama_barang[]"]').val('');
    $('input[name="ukuran_s[]"]').val('0');
    $('input[name="ukuran_m[]"]').val('0');
    $('input[name="ukuran_l[]"]').val('0');
    $('input[name="ukuran_xl[]"]').val('0');
    $('input[name="ukuran_xxl[]"]').val('0');
    $('input[name="jumlah[]"]').val('0');
    $('textarea[name="keterangan_detail[]"]').val('');
}

$(document).ready(function(){
	csrf();
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'surat-jalan/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
                        var button = '<button id="'+ response.data[x].id +'" name="btn_print" class="btn btn-default btn-xs btn-flat" title="Cetak Surat Jalan"><i class="fa fa-print"></i></button> <button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button> <button id="'+ response.data[x].id +'" name="btn_delete" class="btn btn-danger btn-xs btn-flat" title="Hapus Data"><i class="fa fa-trash"></i></button>';

	            		row.push({
	            			'no'            : i,
                            'nomor'         : response.data[x].nomor,
                            'tanggal'       : response.data[x].tanggal,
                            'jumlah_koli'   : response.data[x].jumlah_koli,
                            'harga'         : response.data[x].harga,
                            'keterangan'    : response.data[x].keterangan,
	            			'aksi'          : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
            { 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'tanggal' },
            { 'data' : 'jumlah_koli' },
            { 'data' : 'harga' },
            { 'data' : 'keterangan' },
        	{ 'data' : 'aksi' }
        ],

        // 'order' 	: [[ 2, 'DESC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 6 ]
    		}
  		]
	});

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
});

$('button[name="btn_add"]').click(function(){
    csrf();
    initComponent()
	$('button[name="btn_save"]').attr('id', '0');

    $('#formTitle').text('Tambah Data');

	$('#table').hide();
	setTimeout(function(){
		$('#form').fadeIn()
	}, 100);
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
    csrf();
    initComponent()
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'surat-jalan/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;
                var detail = response.detail;
                
                $('input[name="nomor"]').val(d.nomor);
                $('input[name="tanggal"]').val(d.tanggal);
                $('input[name="jumlah_koli"]').val(d.jumlah_koli);
                $('input[name="harga"]').val(d.harga);
                $('textarea[name="keterangan"]').val(d.keterangan);
                for (let index = 0; index < detail.length; index++) {
                    if (index == 0) {
                        $('input[name="no_order[]"]:eq(0)').val(detail[index].no_order);
                        $('input[name="nama_barang[]"]:eq(0)').val(detail[index].nama_barang);
                        $('input[name="ukuran_s[]"]:eq(0)').val(detail[index].ukuran_s);
                        $('input[name="ukuran_m[]"]:eq(0)').val(detail[index].ukuran_m);
                        $('input[name="ukuran_l[]"]:eq(0)').val(detail[index].ukuran_l);
                        $('input[name="ukuran_xl[]"]:eq(0)').val(detail[index].ukuran_xl);
                        $('input[name="ukuran_xxl[]"]:eq(0)').val(detail[index].ukuran_xxl);
                        $('input[name="jumlah[]"]:eq(0)').val(detail[index].jumlah);
                        $('textarea[name="keterangan_detail[]"]:eq(0)').val(detail[index].keterangan);
                    } else{
                        $('button[name="btn_add_detail"]').trigger('click');
                        $('input[name="no_order[]"]:eq('+index+')').val(detail[index].no_order);
                        $('input[name="nama_barang[]"]:eq('+index+')').val(detail[index].nama_barang);
                        $('input[name="ukuran_s[]"]:eq('+index+')').val(detail[index].ukuran_s);
                        $('input[name="ukuran_m[]"]:eq('+index+')').val(detail[index].ukuran_m);
                        $('input[name="ukuran_l[]"]:eq('+index+')').val(detail[index].ukuran_l);
                        $('input[name="ukuran_xl[]"]:eq('+index+')').val(detail[index].ukuran_xl);
                        $('input[name="ukuran_xxl[]"]:eq('+index+')').val(detail[index].ukuran_xxl);
                        $('input[name="jumlah[]"]:eq('+index+')').val(detail[index].jumlah);
                        $('textarea[name="keterangan_detail[]"]:eq('+index+')').val(detail[index].keterangan);
                    }
                    
                }

                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_delete"]', function(){
	if (!confirm('Apakah anda yakin?')) {
		return;
	}

	var id = $(this).attr('id');

	$.ajax({
        type: 'POST',
        url: baseurl + 'surat-jalan/delete/',
        data: {
        	'id': id,
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                table.ajax.reload(null, false);
				csrf();
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
    initComponent();

	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input, textarea').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'surat-jalan/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
				setTimeout(function(){
					$('#table').fadeIn();
				}, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$(document).on('click', 'button[name="btn_add_detail"]', function(e){
    e.preventDefault();
    var template = $('template[name="detail"]').html();
    $('.list-detail').append(template);
});

$(document).on('click', 'button[name="btn_delete_detail"]', function(e){
    e.preventDefault();
    $(this).closest('.row').remove();
});

$(document).on('change', 'input[name^="ukuran"], input[name="harga[]"]', function(){
    var s = parseInt($(this).closest('.baris').find('input[name="ukuran_s[]"]').val());
    var m = parseInt($(this).closest('.baris').find('input[name="ukuran_m[]"]').val());
    var l = parseInt($(this).closest('.baris').find('input[name="ukuran_l[]"]').val());
    var xl = parseInt($(this).closest('.baris').find('input[name="ukuran_xl[]"]').val());
    var xxl = parseInt($(this).closest('.baris').find('input[name="ukuran_xxl[]"]').val());
    var jumlah = s+m+l+xl+xxl;
    $(this).closest('.baris').find('input[name="jumlah[]"]').val(jumlah);
});

$('#dataTable').on('click', 'button[name="btn_print"]', function(){
    var id = $(this).attr('id');
    window.open(baseurl + 'cetak/surat-jalan/' + id);
});