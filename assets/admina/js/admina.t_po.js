var table = '';
var selectPo = '';

$('.li-master').addClass('menu-open');
$('.li-master .treeview-menu').css('display', 'block');
$('.li-transaksi-po').addClass('active');

function csrf()
{
	$.ajax({
        type: 'GET',
        url: baseurl + 'csrf/get/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var csrf = response.csrf;
				$('input[name="'+ csrf.name +'"]').val(csrf.hash);
            }
        }
    });
}

$(document).ready(function(){
	csrf();
	table = $('#dataTable').DataTable({
		'processing'	: true,
        'serverSide'	: true,

        'ajax' : {
        	'url'	: baseurl + 'transaksi-po/datatable/',
            'type'	: 'GET',
            'dataSrc' : function(response){
            	var i = response.start;
            	var row = new Array();
            	if (response.result) {
            		for(var x in response.data){
            			var button = '<button id="'+ response.data[x].id +'" name="btn_edit" class="btn btn-info btn-xs btn-flat" title="Edit Data"><i class="fa fa-edit"></i></button> <button id="'+ response.data[x].id +'" name="btn_delete" class="btn btn-danger btn-xs btn-flat" title="Hapus Data"><i class="fa fa-trash"></i></button>';

	            		row.push({
	            			'no'            : i,
                            'nomor'         : response.data[x].nomor,
                            'tanggal'       : response.data[x].tanggal,
                            'jatuh_tempo'   : response.data[x].jatuh_tempo,
                            'id_m_po'       : response.data[x].id_m_po,
                            'ukuran_s'      : response.data[x].ukuran_s,
                            'ukuran_m'      : response.data[x].ukuran_m,
                            'ukuran_l'      : response.data[x].ukuran_l,
                            'jumlah'        : response.data[x].jumlah,
                            'harga'         : response.data[x].harga,
                            'total'         : response.data[x].total,
                            'dp'            : response.data[x].dp,
                            'pelunasan'     : response.data[x].pelunasan,
	            			'aksi'	        : button
	            		});
	            		i = i + 1;
	            	}

	            	response.data = row;
            		return row;
            	} else{
            		response.draw = 0;
            		return [];
            	}
            }
        },

        'columns' : [
        	{ 'data' : 'no' },
            { 'data' : 'nomor' },
            { 'data' : 'tanggal' },
            { 'data' : 'jatuh_tempo' },
            { 'data' : 'id_m_po' },
            { 'data' : 'ukuran_s' },
            { 'data' : 'ukuran_m' },
            { 'data' : 'ukuran_l' },
            { 'data' : 'jumlah' },
            { 'data' : 'harga' },
            { 'data' : 'total' },
            { 'data' : 'dp' },
            { 'data' : 'pelunasan' },
        	{ 'data' : 'aksi' }
        ],

        'order' 	: [[ 1, 'ASC' ]],

		'columnDefs': [
    		{
    			'orderable'	: false,
    			'targets'	: [ 0, 13 ]
    		}
  		]
	});

    $.ajax({
        type: 'GET',
        url: baseurl + 'master_po/select/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                $('select[name="id_m_po"]').append('<option value="0">Pilih ID PO</option>');
                for(var x in response.data){
                    $('select[name="id_m_po"]').append('<option value="'+ response.data[x].id +'">'+response.data[x].id_m_po+'</option>');
                }
            }
        }
    });
    selectSupplier = $('select[name="id_supplier"]').select2();

});

$('button[name="btn_add"]').click(function(){
	csrf();
	$('button[name="btn_save"]').attr('id', '0');
    $('input[name="nomor"]').val('');
    $('input[name="tanggal"]').val('');
    $('input[name="jatuh_tempo"]').val('');
    $('input[name="id_m_po"]').val('');
    $('input[name="ukuran_s"]').val('');
    $('input[name="ukuran_m"]').val('');
    $('input[name="ukuran_l"]').val('');
    $('input[name="jumlah"]').val('');
    $('input[name="harga"]').val('');
    $('input[name="total"]').val('');
    $('input[name="dp"]').val('');
    $('input[name="pelunasan"]').val('');


    $('#formTitle').text('Tambah Data');

	$('#table').hide();
	setTimeout(function(){
		$('#form').fadeIn()
	}, 100);
});

$('#dataTable').on('click', 'button[name="btn_edit"]', function(){
	csrf();
	var id = $(this).attr('id');

    $.ajax({
        type: 'GET',
        url: baseurl + 'transaksi-po/edit/'+ id +'/',
        dataType: 'json',
        success: function(response){
            if(response.result){
                var d = response.data;

                $('input[name="nomor"]').val(d.nomor);
                $('input[name="tanggal"]').val(d.tanggal);
                $('input[name="jatuh_tempo"]').val(d.jatuh_tempo);
                $('input[name="id_m_po"]').val(d.id_m_po);
                $('input[name="ukuran_s"]').val(d.ukuran_s);
                $('input[name="ukuran_m"]').val(d.ukuran_m);
                $('input[name="ukuran_l"]').val(d.ukuran_l);
                $('input[name="jumlah"]').val(d.jumlah);
                $('input[name="harga"]').val(d.harga);
                $('input[name="total"]').val(d.total);
                $('input[name="dp"]').val(d.dp);
                $('input[name="pelunasan"]').val(d.pelunasan);
                $('button[name="btn_save"]').attr('id', id);
                $('#formTitle').text('Edit Data');

                csrf();
                $('#table').hide();
                setTimeout(function(){
                    $('#form').fadeIn()
                }, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('#dataTable').on('click', 'button[name="btn_delete"]', function(){
	if (!confirm('Apakah anda yakin?')) {
		return;
	}

	var id = $(this).attr('id');

	$.ajax({
        type: 'POST',
        url: baseurl + 'transaksi-po/delete/',
        data: {
        	'id': id,
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                table.ajax.reload(null, false);
				csrf();
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});

$('button[name="btn_cancel"]').click(function(){
	$('#form').hide();
	setTimeout(function(){
		$('#table').fadeIn();
	}, 100);
});

$('button[name="btn_save"]').click(function(){
	$(this).attr('disabled', 'disabled');
    var missing = false;
    $('#formData').find('input').each(function(){
        if($(this).prop('required')){
            if($(this).val() == ''){
                var placeholder = $(this).attr('placeholder');
                $.notify({
                    icon: 'glyphicon glyphicon-info-sign',
                    message: 'Kolom ' + placeholder +' tidak boleh kosong.'
                }, {
                    type: 'warning',
                    delay: 1000,
                    timer: 500,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
                $(this).focus();
                missing = true;
                return false;
            }
        }
    });

    $(this).removeAttr('disabled');
    if(missing){
        return;
    }

    $.ajax({
        type: 'POST',
        url: baseurl + 'transaksi-po/save/',
        data: {
        	'id': $(this).attr('id'),
        	'form': $('#formData').serialize(),
			'csrf_token': $('input[id="csrf"]').val()
        },
        dataType: 'json',
        success: function(response){
            if(response.result){
            	$.notify({
                    icon: "glyphicon glyphicon-ok",
                    message: response.msg
                }, {
                    type: 'success',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
				csrf();
                table.ajax.reload(null, false);
                $('#form').hide();
				setTimeout(function(){
					$('#table').fadeIn();
				}, 100);
            } else{
                $.notify({
                    icon: "glyphicon glyphicon-info-sign",
                    message: response.msg
                }, {
                    type: 'danger',
                    delay: 3000,
                    timer: 1000,
                    placement: {
                        from: 'top',
                        align: 'center'
                    }
                });
            }
        }
    });
});
