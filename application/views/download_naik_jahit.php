<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Sewing</title>
    <style>
      .dataTable {
        border-collapse: collapse;
      }
      .dataTable th, .dataTable td{
        border: 1px solid black;
      }
    </style>
</head>
<body>
    <table width="100%">
      <tr>
        <td style="width:50%;">
          <img src="assets/dist/img/agp.png" alt="AGP" style="width:300px;margin-top:-25px;">
        </td>
        <td style="width:50%;">
          <table>
            <tr>
              <td align="right" style="width:45%;"><b>PO. No</b></td>
              <td style="width:5%;">:</td>
              <td style="width:50%;"><?php echo $data->nomor; ?></td>
            </tr>
            <tr>
              <td align="right"><b>INVOICE NO.</b></td>
              <td>:</td>
              <td><?php echo $data->nomor_invoice ?></td>
            </tr>
            <tr>
              <td align="right"><b>ORDER DATE</b></td>
              <td>:</td>
              <td><?php echo date('d F Y', strtotime($data->tanggal)); ?></td>
            </tr>
            <!-- <tr>
              <td align="right"><b>PAYMENT</b></td>
              <td>:</td>
              <td><?php echo $data->nama_payment; ?></td>
            </tr> -->
            <tr>
              <td valign="top" align="right"><b>Bill To</b></td>
              <td valign="top">:</td>
              <td style="word-wrap:break-word;">
                <?php echo $data->nama_customer; ?> <br>
                <?php echo $data->alamat; ?> <br>
                <?php echo $data->kontak; ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <hr color="black" background-color="black" size="3">
    <table width="100%" class="dataTable">
      <thead>
        <tr style="text-align:center;">
          <th>Warna</th>
          <th>Size</th>
          <th>Qty</th>
          <?php
          for ($i=0; $i < count($tanggal); $i++) { 
            $tgl = substr($tanggal[$i],8,2);
            $bln = substr($tanggal[$i],5,2);
          ?>
            <th><?php echo $tgl.'/'.$bln; ?></th>
          <?php }
          ?>
          <th>Total</th>
          <th>Balance</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($detail as $key => $value) { ?>
            <tr style="text-align:center;">
              <td rowspan="5"><?php echo $value->nama_warna; ?></td>
              <td>S</td>
              <td><?php echo $value->cutting[0]->ukuran_s; ?></td>
              <?php
              $total = 0;
              for ($i=0; $i < count($tanggal); $i++) { 
                $progres = $value->progres;
                $nol = true;
                for ($j=0; $j < count($progres); $j++) { 
                  if ($tanggal[$i] == $progres[$j]->tanggal) { ?>
                    <td>
                      <?php 
                        echo $progres[$j]->ukuran_s;
                        $total = $total + $progres[$j]->ukuran_s;
                        $nol = false;
                        break;
                      ?>
                    </td>
                  <?php } 
                }
                if ($nol) { ?>
                  <td>0</td>
                <?php }
              }
              ?>
              <td><?php echo $total; ?></td>
              <?php
              $balance = $value->cutting[0]->ukuran_s - $total;
              if ($balance < 0) {
                $balance = -1 * $balance;
              }
              ?>
              <td><?php echo $balance; ?></td>
            </tr>
            <tr style="text-align:center;">
              <td>M</td>
              <td><?php echo $value->cutting[0]->ukuran_m; ?></td>
              <?php
              $total = 0;
              for ($i=0; $i < count($tanggal); $i++) { 
                $progres = $value->progres;
                $nol = true;
                for ($j=0; $j < count($progres); $j++) { 
                  if ($tanggal[$i] == $progres[$j]->tanggal) { ?>
                    <td>
                      <?php 
                        echo $progres[$j]->ukuran_m;
                        $total = $total + $progres[$j]->ukuran_m;
                        $nol = false;
                        break;
                      ?>
                    </td>
                  <?php } 
                }
                if ($nol) { ?>
                  <td>0</td>
                <?php }
              }
              ?>
              <td><?php echo $total; ?></td>
              <?php
              $balance = $value->cutting[0]->ukuran_m - $total;
              if ($balance < 0) {
                $balance = -1 * $balance;
              }
              ?>
              <td><?php echo $balance; ?></td>
            </tr>
            <tr style="text-align:center;">
              <td>L</td>
              <td><?php echo $value->cutting[0]->ukuran_l; ?></td>
              <?php
              $total = 0;
              for ($i=0; $i < count($tanggal); $i++) { 
                $progres = $value->progres;
                $nol = true;
                for ($j=0; $j < count($progres); $j++) { 
                  if ($tanggal[$i] == $progres[$j]->tanggal) { ?>
                    <td>
                      <?php 
                        echo $progres[$j]->ukuran_l;
                        $total = $total + $progres[$j]->ukuran_l;
                        $nol = false;
                        break;
                      ?>
                    </td>
                  <?php } 
                }
                if ($nol) { ?>
                  <td>0</td>
                <?php }
              }
              ?>
              <td><?php echo $total; ?></td>
              <?php
              $balance = $value->cutting[0]->ukuran_l - $total;
              if ($balance < 0) {
                $balance = -1 * $balance;
              }
              ?>
              <td><?php echo $balance; ?></td>
            </tr>
            <tr style="text-align:center;">
              <td>XL</td>
              <td><?php echo $value->cutting[0]->ukuran_xl; ?></td>
              <?php
              $total = 0;
              for ($i=0; $i < count($tanggal); $i++) { 
                $progres = $value->progres;
                $nol = true;
                for ($j=0; $j < count($progres); $j++) { 
                  if ($tanggal[$i] == $progres[$j]->tanggal) { ?>
                    <td>
                      <?php 
                        echo $progres[$j]->ukuran_xl;
                        $total = $total + $progres[$j]->ukuran_xl;
                        $nol = false;
                        break;
                      ?>
                    </td>
                  <?php } 
                }
                if ($nol) { ?>
                  <td>0</td>
                <?php }
              }
              ?>
              <td><?php echo $total; ?></td>
              <?php
              $balance = $value->cutting[0]->ukuran_xl - $total;
              if ($balance < 0) {
                $balance = -1 * $balance;
              }
              ?>
              <td><?php echo $balance; ?></td>
            </tr>
            <tr style="text-align:center;">
              <td>XXL</td>
              <td><?php echo $value->cutting[0]->ukuran_xxl; ?></td>
              <?php
              $total = 0;
              for ($i=0; $i < count($tanggal); $i++) { 
                $progres = $value->progres;
                $nol = true;
                for ($j=0; $j < count($progres); $j++) { 
                  if ($tanggal[$i] == $progres[$j]->tanggal) { ?>
                    <td>
                      <?php 
                        echo $progres[$j]->ukuran_xxl;
                        $total = $total + $progres[$j]->ukuran_xxl;
                        $nol = false;
                        break;
                      ?>
                    </td>
                  <?php } 
                }
                if ($nol) { ?>
                  <td>0</td>
                <?php }
              }
              ?>
              <td><?php echo $total; ?></td>
              <?php
              $balance = $value->cutting[0]->ukuran_xxl - $total;
              if ($balance < 0) {
                $balance = -1 * $balance;
              }
              ?>
              <td><?php echo $balance; ?></td>
            </tr>
          <?php }
        ?>
      </tbody>
    </table>
    <hr color="black" background-color="black" size="3">
</body>
</html>