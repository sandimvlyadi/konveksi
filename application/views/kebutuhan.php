<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | Kebutuhan</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Kebutuhan</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="table" class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row">
                <div class="col-xs-12">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nomor PO</th>
                      <th>Total</th>
                      <th>Keterangan</th>
                      <th style="min-width: 100px;">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="form" class="row" style="display: none;">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row kebutuhan-detail">
                <div class="col-xs-6"></div>
                <div class="col-xs-6"></div>
                <div class="col-xs-6"></div>
              </div>
              <div class="row kebutuhan-detail-table">
                <div class="col-xs-12">
                  <table id="dataTableDetail" class="table table-bordered table-striped">
                    <thead>
                      <th class="bg-primary">Warna</th>
                      <th class="bg-primary">S</th>
                      <th class="bg-primary">M</th>
                      <th class="bg-primary">L</th>
                      <th class="bg-primary">XL</th>
                      <th class="bg-primary">XXL</th>
                      <th class="bg-primary">QTY</th>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
              <form id="formData">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="form-group col-md-12 list-bahan-baku">
                  <label><h3>Daftar Kebutuhan</h3></label>
                  <div class="row">
                    <div class="col-md-1">
                      <label>Tambah</label>
                      <button name="btn_add_bahan_baku" class="btn btn-success btn-flat btn-block"><i class="fa fa-plus"></i></button>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Bahan Baku</label>
                      <select name="id_m_bahan_baku[]" class="form-control" style="width: 100%;">
                        <option value="0">- Pilih Bahan Baku -</option>
                        <?php
                        foreach ($bahan_baku as $key => $value) { ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_bahan_baku']; ?></option>
                        <?php }
                        ?>
                      </select>
                    </div>
                    <div class="form-group col-md-1">
                      <label>Qty</label>
                      <input type="number" name="qty[]" class="form-control" placeholder="Qty" required>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Satuan</label>
                      <select name="id_m_satuan[]" class="form-control" style="width: 100%;">
                        <option value="0">- Pilih Satuan -</option>
                        <?php
                        foreach ($satuan as $key => $value) { ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_satuan']; ?></option>
                        <?php }
                        ?>
                      </select>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Harga</label>
                      <input type="number" name="harga[]" class="form-control" placeholder="Harga" required>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Ceklist</label>
                      <select name="status[]" class="form-control" style="width:100%;">
                        <option value="0">Belum</option>
                        <option value="1">Sudah</option>
                      </select>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Keterangan</label>
                      <textarea name="keterangan_bahan_baku[]" class="form-control" placeholder="Keterangan"></textarea>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="box-footer">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <button id="0" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                  <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <template name="bahan_baku">
    <div class="row">
      <div class="col-md-1">
        <label>Hapus</label>
        <button name="btn_delete_bahan_baku" class="btn btn-danger btn-flat btn-block"><i class="fa fa-trash"></i></button>
      </div>
      <div class="form-group col-md-2">
        <label>Bahan Baku</label>
        <select name="id_m_bahan_baku[]" class="form-control" style="width: 100%;">
          <option value="0">- Pilih Bahan Baku -</option>
          <?php
          foreach ($bahan_baku as $key => $value) { ?>
            <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_bahan_baku']; ?></option>
          <?php }
          ?>
        </select>
      </div>
      <div class="form-group col-md-1">
        <label>Qty</label>
        <input type="number" name="qty[]" class="form-control" placeholder="Qty" required>
      </div>
      <div class="form-group col-md-2">
        <label>Satuan</label>
        <select name="id_m_satuan[]" class="form-control" style="width: 100%;">
          <option value="0">- Pilih Satuan -</option>
          <?php
          foreach ($satuan as $key => $value) { ?>
            <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_satuan']; ?></option>
          <?php }
          ?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <label>Harga</label>
        <input type="number" name="harga[]" class="form-control" placeholder="Harga" required>
      </div>
      <div class="form-group col-md-2">
        <label>Ceklist</label>
        <select name="status[]" class="form-control" style="width:100%;">
          <option value="0">Belum</option>
          <option value="1">Sudah</option>
        </select>
      </div>
      <div class="form-group col-md-2">
        <label>Keterangan</label>
        <textarea name="keterangan_bahan_baku[]" class="form-control" placeholder="Keterangan"></textarea>
      </div>
    </div>
  </template>

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.kebutuhan.js'); ?>"></script>
</body>
</html>