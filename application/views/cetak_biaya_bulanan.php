<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php

function blnChanger($bln)
{
	$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

	return $bulan[intval($bln) - 1];
}

?>

<!DOCTYPE html>
<html>
<head>
  <title>Laporan Biaya Bulanan</title>
	<style type="text/css">
	body {
	  counter-reset: section;
	}
	@media screen{
		div.tutup{display: none;}
	}
	@media print {  
	    thead {display: table-header-group; border-top: 2px #000 solid; }   
	    div.tutup{display:block; background-color: #fff; color: #fff; width: 98.5%; height: 34px; margin-top: -15px; position: absolute; border-bottom: #000 2px solid; }
	    table.gridtable{margin-top: -15px;}
	} 
	@page {
	  margin: 4%;
	}
	@page {
	    counter-increment: page;
	    counter-reset: page 1;
	    @top-right {
	        content: "Page " counter(page) " of " counter(pages);
	    }
	}
	body{ font-family: "Calibri Body", Arial, Helvetica, sans-serif, margin: 0;}
	h3{font-size: 1em; font-weight: bold;}
	p{font-size: .95em; font-weight: bold;}
	table {
	  font-family: "Calibri Body", Arial, Helvetica, sans-serif;;
	  font-size:0.8em;
	  color:#333333;
	  border-width: 1px;
	  border-color: #000;
	  border-collapse: collapse;  
	}
	
	</style>
</head>
<body onload="print();">

<table align="center" border="0" width="100%">
	<tr align="center">
		<td>
			<table align="center" width="100%" border="0" cellpadding="5">
				<tr>
					<td>
						<img src="<?php echo base_url('assets/dist/img/agp.png'); ?>" alt="AGP" style="width:300px;margin-top:10px;">
					</td>
				</tr>
			</table>

		</td>
	</tr>
	<tr align="center"><td colspan="4"><b>LAPORAN BIAYA BULANAN</b></td></tr>

	<tr>
		<td colspan="4">
			<hr color="black" background-color="black" size="2">
			<table align="center" width="100%" border="0" cellpadding="5">
				<tbody>
					<tr>
						<td>
							<b>BULAN</b> : <?php echo blnChanger($bulan) . " " . $tahun; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<hr color="black" background-color="black" size="2">
			<p></p>
			<table id="tableLaporanBulanan" align="center" width="100%" border="1" cellpadding="3">
        <thead align="center">
          <th>No.</th>
          <th>Nama Biaya</th>
          <th>Nominal</th>
          <th>Tanggal</th>
          <th>Keterangan</th>
        </thead>
        <tbody>
        	<?php
        	if (count($data) > 0) {
        		$total = 0;
        		for ($i=0; $i < count($data); $i++) { ?>
        			<?php $total = $total + $data[$i]['nominal']; ?>
	        		<tr>
	        			<td align="center"><?php echo $i+1; ?></td>
	        			<td><?php echo $data[$i]['nama_biaya']; ?></td>
	        			<td><?php echo $data[$i]['nominal']; ?></td>
	        			<td><?php echo $data[$i]['tanggal']; ?></td>
	        			<td><?php echo $data[$i]['keterangan']; ?></td>
	        		</tr>
	        	<?php }
        	} else{ ?>
        		<tr>
        			<td colspan="7" align="center">Tidak ada transaksi pada bulan ini.</td>
        		</tr>
        	<?php }
        	?>
        </tbody>
      </table>
      <p></p>
      <hr color="black" background-color="black" size="2">
		</td>
	  <tr height="50">
          <td colspan="11"></td>
        </tr>
        <tr style="font-weight:bold;">
          <td colspan="1"></td>
          <td style="border-bottom:1px solid black;">Total &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="border-bottom:1px solid black;">Rp.</td>
          <td style="border-bottom:1px solid black;"><?php  echo number_format($total,2,',','.'); ?></td>
        </tr>
	</tr>
</table>
</body>
</html>