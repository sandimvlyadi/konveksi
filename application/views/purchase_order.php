<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | Purchase Order</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Purchase Order</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="table" class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row" style="padding-bottom: 10px;">
                <div class="col-xs-12">
                  <button name="btn_add" class="btn btn-xs btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nomor PO</th>
                      <th>Tanggal</th>
                      <th>Customer</th>
                      <th>Item</th>
                      <th>Keterangan</th>
                      <th style="min-width: 125px;">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="form" class="row" style="display: none;">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 id="formTitle">Tambah Data</h3>
            </div>
            <div class="box-body">
              <form id="formData">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="form-group col-md-3">
                  <label>Nomor PO</label>
                  <input type="text" name="nomor" class="form-control" placeholder="Nomor PO" required>
                </div>
                <div class="form-group col-md-3">
                  <label>No. Invoice</label>
                  <input type="text" name="nomor_invoice" class="form-control" placeholder="No. Invoice" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Date</label>
                  <input type="text" name="tanggal" class="form-control datepicker" placeholder="Date" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Customer</label>
                  <select name="id_m_customer" class="form-control" style="width:100%;"></select>
                </div>
                <div class="form-group col-md-3">
                  <label>Style</label>
                  <input type="text" name="nama_item" class="form-control" placeholder="Style" required>
                </div>
                <!-- <div class="form-group col-md-3">
                  <label>Metoda Pembayaran</label>
                  <select name="id_m_payment" class="form-control" style="width:100%;"></select>
                </div> -->
                <div class="form-group col-md-3">
                  <label>Down Payment (DP)</label>
                  <input type="text" name="dp" class="form-control inputmask" placeholder="Down Payment (DP)" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Total Qty</label>
                  <input type="number" name="total_qty" class="form-control" placeholder="Total Qty" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label>Total Pembayaran</label>
                  <input type="text" name="total_pembayaran" class="form-control inputmask" placeholder="Total Pembayaran" readonly>
                </div>
                <div class="form-group col-md-12">
                  <label>Keterangan</label>
                  <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                </div>

                <div class="form-group col-md-12 customer" style="display: none;">
                  <label><h3>Detail Customer Baru</h3></label>
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label>Nama</label>
                      <input type="text" name="nama_customer" class="form-control" placeholder="Nama Customer Baru" readonly>
                    </div>
                    <div class="form-group col-md-4">
                      <label>Alamat</label>
                      <input type="text" name="alamat_customer" class="form-control" placeholder="Alamat">
                    </div>
                    <div class="form-group col-md-4">
                      <label>Kontak</label>
                      <input type="text" name="kontak_customer" class="form-control" placeholder="Kontak">
                    </div>
                    <div class="form-group col-md-12">
                      <label>Keterangan</label>
                      <textarea name="keterangan_customer" class="form-control" placeholder="Keterangan"></textarea>
                    </div>
                  </div>
                </div>
                
                <div class="form-group col-md-12 list-warna">
                  <label><h3>Detail Warna</h3></label>
                  <div class="row baris">
                    <div class="col-md-1">
                      <label>Tambah</label>
                      <button name="btn_add_warna" class="btn btn-success btn-flat btn-block"><i class="fa fa-plus"></i></button>
                    </div>
                    <div class="form-group col-md-3">
                      <label>Warna</label>
                      <select name="id_m_warna[]" class="form-control" style="width: 100%;">
                        <option value="0">- Pilih Warna -</option>
                        <?php
                        foreach ($warna as $key => $value) { ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_warna']; ?></option>
                        <?php }
                        ?>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <div class="row">
                        <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
                          <label>Ukuran S</label>
                          <input type="number" name="ukuran_s[]" class="form-control" placeholder="Ukuran S" required>
                        </div>
                        <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
                          <label>Ukuran M</label>
                          <input type="number" name="ukuran_m[]" class="form-control" placeholder="Ukuran M" required>
                        </div>
                        <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
                          <label>Ukuran L</label>
                          <input type="number" name="ukuran_l[]" class="form-control" placeholder="Ukuran L" required>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
                          <label>Ukuran XL</label>
                          <input type="number" name="ukuran_xl[]" class="form-control" placeholder="Ukuran XL" required>
                        </div>
                        <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
                          <label>Ukuran XXL</label>
                          <input type="number" name="ukuran_xxl[]" class="form-control" placeholder="Ukuran XXL" required>
                        </div>
                        <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
                          <label>QTY</label>
                          <input type="number" name="qty[]" class="form-control" placeholder="QTY" readonly required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2" style="padding-left: 3px;">
                      <div class="row">
                        <div class="form-group col-md-12">
                          <label>Harga</label>
                          <input type="text" name="harga[]" class="form-control inputmask" placeholder="Harga" required>
                        </div>
                        <div class="form-group col-md-12">
                          <label>Total</label>
                          <input type="text" name="total[]" class="form-control inputmask" placeholder="Total" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-3">
                      <label>Keterangan</label>
                      <textarea name="keterangan_warna[]" class="form-control" placeholder="Keterangan"></textarea>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="box-footer">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <button id="0" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                  <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <template name="warna">
    <div class="row baris">
      <div class="col-md-1">
        <label>Hapus</label>
        <button name="btn_delete_warna" class="btn btn-danger btn-flat btn-block"><i class="fa fa-trash"></i></button>
      </div>
      <div class="form-group col-md-3">
        <label>Warna</label>
        <select name="id_m_warna[]" class="form-control" style="width: 100%;">
          <option value="0">- Pilih Warna -</option>
          <?php
          foreach ($warna as $key => $value) { ?>
            <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_warna']; ?></option>
          <?php }
          ?>
        </select>
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
            <label>Ukuran S</label>
            <input type="number" name="ukuran_s[]" class="form-control" placeholder="Ukuran S" required>
          </div>
          <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
            <label>Ukuran M</label>
            <input type="number" name="ukuran_m[]" class="form-control" placeholder="Ukuran M" required>
          </div>
          <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
            <label>Ukuran L</label>
            <input type="number" name="ukuran_l[]" class="form-control" placeholder="Ukuran L" required>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
            <label>Ukuran XL</label>
            <input type="number" name="ukuran_xl[]" class="form-control" placeholder="Ukuran XL" required>
          </div>
          <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
            <label>Ukuran XXL</label>
            <input type="number" name="ukuran_xxl[]" class="form-control" placeholder="Ukuran XXL" required>
          </div>
          <div class="form-group col-xs-4 col-md-4" style="padding-right:3px;padding-left:3px;">
            <label>QTY</label>
            <input type="number" name="qty[]" class="form-control" placeholder="QTY" readonly required>
          </div>
        </div>
      </div>
      <div class="col-md-2" style="padding-left: 3px;padding-right: 3px;">
        <div class="row">
          <div class="form-group col-md-12">
            <label>Harga</label>
            <input type="text" name="harga[]" class="form-control inputmask" placeholder="Harga" value="0" required>
          </div>
          <div class="form-group col-md-12">
            <label>Total</label>
            <input type="text" name="total[]" class="form-control inputmask" placeholder="Total" value="0" readonly>
          </div>
        </div>
      </div>
      <div class="form-group col-md-3">
        <label>Keterangan</label>
        <textarea name="keterangan_warna[]" class="form-control" placeholder="Keterangan"></textarea>
      </div>
    </div>
  </template>

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.purchase.order.js'); ?>"></script>
</body>
</html>