<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Surat Jalan</title>
</head>
<body onload="print()">
    <table width="100%">
      <tr>
        <td>
          <img src="<?php echo base_url('assets/dist/img/agp.png'); ?>" alt="AGP" style="width:300px;margin-top:10px;">
        </td>
        <td>
          <table>
            <tr>
              <td align="right"><b>NOMOR</b></td>
              <td>:</td>
              <td style="width:200px;"><?php echo $data->nomor; ?></td>
            </tr>
            <tr>
              <td align="right"><b>TANGGAL</b></td>
              <td>:</td>
              <td><?php echo date('d F Y', strtotime($data->tanggal)); ?></td>
            </tr>
            <tr>
              <td align="right"><b>JUMLAH KOLI</b></td>
              <td>:</td>
              <td><?php echo $data->jumlah_koli . ' Koli'; ?></td>
            </tr>
            <tr>
              <td align="right"><b>HARGA</b></td>
              <td>:</td>
              <td><?php echo 'Rp. ' . number_format($data->harga,0,',',',') . ',-'; ?></td>
            </tr>
          </table>
        </td>
        <tr>
          <td colspan="2"><h3 style="margin-left:25px;">SURAT JALAN</h3></td>
        </tr>
      </tr>
    </table>
    <hr color="black" background-color="black" size="3">
    <table width="100%">
      <thead>
        <tr>
          <th>NO. ORDER</th>
          <th>NAMA BARANG</th>
          <th>S</th>
          <th>M</th>
          <th>L</th>
          <th>XL</th>
          <th>XXL</th>
          <th>JML</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($detail as $key => $value) { ?>
            <tr>
              <td align="center"><?php echo $value->no_order; ?></td>
              <td><?php echo $value->nama_barang; ?></td>
              <td align="center"><?php if(intval($value->ukuran_s)>0) echo $value->ukuran_s; ?></td>
              <td align="center"><?php if(intval($value->ukuran_m)>0) echo $value->ukuran_m; ?></td>
              <td align="center"><?php if(intval($value->ukuran_l)>0) echo $value->ukuran_l; ?></td>
              <td align="center"><?php if(intval($value->ukuran_xl)>0) echo $value->ukuran_xl; ?></td>
              <td align="center"><?php if(intval($value->ukuran_xxl)>0) echo $value->ukuran_xxl; ?></td>
              <td align="center"><?php if(intval($value->jumlah)>0) echo $value->jumlah; ?></td>
            </tr>
          <?php }
        ?>
      </tbody>
    </table>
    
    <hr color="black" background-color="black" size="3">
    <table cellspacing="25" width="100%" style="margin-top:25px;">
      <tr align="center">
        <td></td>
        <td>Gudang</td>
        <td></td>
        <td>Driver</td>
        <td></td>
        <td>Penerima</td>
      </tr>
      <tr align="center" height="75">
        <td></td>
        <td style="border-bottom:1px solid black;"></td>
        <td></td>
        <td style="border-bottom:1px solid black;"></td>
        <td></td>
        <td style="border-bottom:1px solid black;"></td>
      </tr>
    </table>
</body>
</html>