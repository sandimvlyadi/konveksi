<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Biaya Bulanan</title>
</head>
<body onload="print()">
    <table width="100%">
      <tr>
        <td>
          <img src="<?php echo base_url('assets/dist/img/agp.png'); ?>" alt="AGP" style="width:300px;margin-top:10px;">
        </td>

        <tr>
          <td colspan="2"><h3 style="margin-left:25px;">Biaya Bulan : </h3></td>
        </tr>
        <tr>
          <td colspan="2"><h3 style="margin-left:25px;"><?php echo date('F', strtotime($data->tanggal)); ?></td>
        </tr>
      </tr>
    </table>

    <hr color="black" background-color="black" size="3">
    <table width="100%">
      <thead>
        <tr>
          <th>Nama Biaya</th>
          <th>Nominal</th>
          <th>Tanggal</th>
          <th>Keterangan</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($detail as $key => $value) { ?>
            <tr>
              <td align="center"><?php echo $value->nama_biaya; ?></td>
              <td align="center"><?php echo $value->nominal; ?></td>
              <td align="center"><?php echo date('d F Y', strtotime($value->tanggal)); ?></td>
              <td align="center"><?php echo $value->keterangan; ?></td>
            </tr>
          <?php }
        ?>
      </tbody>
    </table>
    <hr color="black" background-color="black" size="3">

    <table width="75%" style="font-weight:bold;margin-top:25px;">
      <tr>
        <td align="center">TRANSFER BANK</td>
        <td></td>
      </tr>
      <tr>
        <td align="right" style="border-right:1px solid black;">
          BCA 2781467776 <br>
          BRI 1752 01 002841 501 <br>
          MANDIRI 1320010570894 <br>
        </td>
        <td>
          &nbsp;&nbsp;&nbsp; a.n Asep Mochdar
        </td>
      </tr>
    </table>
</body>
</html>