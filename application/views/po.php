<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | PO</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>PO</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="table" class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row" style="padding-bottom: 10px;">
                <div class="col-xs-12">
                  <button name="btn_add" class="btn btn-xs btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nomor</th>
                      <th>Customer</th>
                      <th>Tanggal</th>
                      <th>Jatuh Tempo</th>
                      <th>Nama PO</th>
                      <th>Jumlah</th>
                      <th>Total</th>
                      <th>DP</th>
                      <th>Pelunasan</th>
                      <th>Keterangan</th>
                      <th style="min-width: 75px;">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="form" class="row" style="display: none;">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 id="formTitle">Tambah Data</h3>
            </div>
            <div class="box-body">
              <form id="formData">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="form-group col-md-2">
                  <label>Nomor</label>
                  <input type="text" name="nomor" class="form-control" placeholder="Nomor" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Customer</label>
                  <select name="id_m_customer" class="form-control" style="width: 100%;"></select>
                </div>
                <div class="form-group col-md-3">
                  <label>Nama PO</label>
                  <select name="id_m_po" class="form-control" style="width: 100%;"></select>
                </div>
                <div class="form-group col-md-2">
                  <label>Tanggal</label>
                  <input type="text" name="tanggal" class="form-control datepicker" placeholder="Tanggal" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Jatuh Tempo</label>
                  <input type="text" name="jatuh_tempo" class="form-control datepicker" placeholder="Jatuh Tempo" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Ukuran S</label>
                  <input type="number" name="ukuran_s" class="form-control" placeholder="Jumlah S" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Ukuran M</label>
                  <input type="number" name="ukuran_m" class="form-control" placeholder="Jumlah M" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Ukuran L</label>
                  <input type="number" name="ukuran_l" class="form-control" placeholder="Jumlah L" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Ukuran XL</label>
                  <input type="number" name="ukuran_xl" class="form-control" placeholder="Jumlah XL" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Ukuran XXL</label>
                  <input type="number" name="ukuran_xxl" class="form-control" placeholder="Jumlah XXL" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Jumlah</label>
                  <input type="number" name="jumlah" class="form-control" placeholder="Jumlah Order" required readonly>
                </div>
                <div class="form-group col-md-6">
                  <label>Harga</label>
                  <input type="number" name="harga" class="form-control" placeholder="Harga Satuan" required>
                </div>
                <div class="form-group col-md-6">
                  <label>Total</label>
                  <input type="number" name="total" class="form-control" placeholder="Total" required readonly>
                </div>
                <div class="form-group col-md-6">
                  <label>DP</label>
                  <input type="number" name="dp" class="form-control" placeholder="DP" required>
                </div>
                <div class="form-group col-md-6">
                  <label>Pelunasan</label>
                  <input type="number" name="pelunasan" class="form-control" placeholder="Sisa Pelunasan" required readonly>
                </div>
                <div class="form-group col-md-12">
                  <label>Keterangan</label>
                  <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                </div>
              </form>
            </div>
            <div class="box-footer">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <button id="0" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                  <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.po.js'); ?>"></script>
</body>
</html>