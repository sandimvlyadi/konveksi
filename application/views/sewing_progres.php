<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | Sewing Progres</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Sewing Progres</h1>
    </section>

    <!-- Main content -->
    <section class="content">

    <div id="form" class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-body">
            <div class="row progres-detail">
              <div class="col-xs-6">
                NO. PO : <strong><?php echo $data['nomor']; ?></strong>
              </div>
              <div class="col-xs-6">
                NO. INVOICE : <strong><?php echo $data['nomor_invoice']; ?></strong>
              </div>
              <div class="col-xs-6">
                STYLE : <strong><?php echo $data['nama_item']; ?></strong>
              </div>
            </div>
          </div>
          <div class="row" style="margin:0px;">
            <div class="col-xs-12">
              <form id="formData" autocomplete="off">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="table-responsive">
                  <table class="table table-bordered" style="text-align:center;min-width:1024px;">
                    <thead>
                      <tr>
                        <th style="text-align:center;">No.</th>
                        <th style="text-align:center;">Warna</th>
                        <th style="text-align:center;">S</th>
                        <th style="text-align:center;">M</th>
                        <th style="text-align:center;">L</th>
                        <th style="text-align:center;">XL</th>
                        <th style="text-align:center;">XXL</th>
                        <th style="text-align:center;">QTY</th>
                        <th style="text-align:center;">
                          <div class="row list-progres">
                            <div class="col-xs-1">S</div>
                            <div class="col-xs-1">M</div>
                            <div class="col-xs-1">L</div>
                            <div class="col-xs-1">XL</div>
                            <div class="col-xs-1">XXL</div>
                            <div class="col-xs-1">QTY</div>
                            <div class="col-xs-2">Balance</div>
                            <div class="col-xs-1"></div>
                            <div class="col-xs-2">Tanggal</div>
                            <div class="col-xs-1"></div>
                          </div>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        $no = 1;
                        foreach ($detail as $kDetail => $vDetail) { ?>
                          <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $vDetail->nama_warna; ?></td>
                            <td><?php echo $vDetail->ukuran_s; ?></td>
                            <td><?php echo $vDetail->ukuran_m; ?></td>
                            <td><?php echo $vDetail->ukuran_l; ?></td>
                            <td><?php echo $vDetail->ukuran_xl; ?></td>
                            <td><?php echo $vDetail->ukuran_xxl; ?></td>
                            <td>
                              <?php echo $vDetail->qty; ?>
                              <input type="hidden" name="qty_warna" value="<?php echo $vDetail->qty; ?>">
                            </td>
                            <td class="baris">
                              <?php
                                if (count($vDetail->progres) > 0) {
                                  $i = 0;
                                  foreach ($vDetail->progres as $kProgres => $vProgres) {
                                    if ($i == 0) { ?>
                                      <div class="row list-progres" style="margin-bottom:5px;">
                                        <div class="col-xs-1">
                                          <input type="hidden" name="progres_id[]" value="<?php echo $vProgres->id; ?>">
                                          <input type="hidden" name="id_m_warna[]" value="<?php echo $vDetail->id_m_warna; ?>">
                                          <input type="number" name="ukuran_s[]" value="<?php echo $vProgres->ukuran_s; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_m[]" value="<?php echo $vProgres->ukuran_m; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_l[]" value="<?php echo $vProgres->ukuran_l; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_xl[]" value="<?php echo $vProgres->ukuran_xl; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_xxl[]" value="<?php echo $vProgres->ukuran_xxl; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="qty[]" readonly>
                                        </div>
                                        <div class="col-xs-2">
                                          <input type="number" name="balance[]" readonly>
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="qty_balance[]" readonly>
                                        </div>
                                        <div class="col-xs-2">
                                          <input type="text" name="tanggal[]" class="datepicker" value="<?php echo $vProgres->tanggal; ?>" required>
                                        </div>
                                        <div class="col-xs-1">
                                          <button type="button" name="btn_add_progres" class="btn btn-xs btn-flat btn-success">
                                            <i class="fa fa-plus"></i>
                                          </button>
                                        </div>
                                      </div>
                                    <?php } else{ ?>
                                      <div class="row list-progres" style="margin-bottom:5px;">
                                        <div class="col-xs-1">
                                          <input type="hidden" name="progres_id[]" value="<?php echo $vProgres->id; ?>">
                                          <input type="hidden" name="id_m_warna[]" value="<?php echo $vDetail->id_m_warna; ?>">
                                          <input type="number" name="ukuran_s[]" value="<?php echo $vProgres->ukuran_s; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_m[]" value="<?php echo $vProgres->ukuran_m; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_l[]" value="<?php echo $vProgres->ukuran_l; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_xl[]" value="<?php echo $vProgres->ukuran_xl; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="ukuran_xxl[]" value="<?php echo $vProgres->ukuran_xxl; ?>">
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="qty[]" readonly>
                                        </div>
                                        <div class="col-xs-2">
                                          <input type="number" name="balance[]" readonly>
                                        </div>
                                        <div class="col-xs-1">
                                          <input type="number" name="qty_balance[]" readonly>
                                        </div>
                                        <div class="col-xs-2">
                                          <input type="text" name="tanggal[]" class="datepicker" value="<?php echo $vProgres->tanggal; ?>" required>
                                        </div>
                                        <div class="col-xs-1">
                                          <button type="button" name="btn_delete_progres" class="btn btn-xs btn-flat btn-danger">
                                            <i class="fa fa-trash"></i>
                                          </button>
                                        </div>
                                      </div>
                                    <?php }
                                    $i = $i + 1;
                                  }
                                } else{ ?>
                                  <div class="row list-progres" style="margin-bottom:5px;">
                                    <div class="col-xs-1">
                                      <input type="hidden" name="progres_id[]" value="0">
                                      <input type="hidden" name="id_m_warna[]" value="<?php echo $vDetail->id_m_warna; ?>">
                                      <input type="number" name="ukuran_s[]">
                                    </div>
                                    <div class="col-xs-1">
                                      <input type="number" name="ukuran_m[]">
                                    </div>
                                    <div class="col-xs-1">
                                      <input type="number" name="ukuran_l[]">
                                    </div>
                                    <div class="col-xs-1">
                                      <input type="number" name="ukuran_xl[]">
                                    </div>
                                    <div class="col-xs-1">
                                      <input type="number" name="ukuran_xxl[]">
                                    </div>
                                    <div class="col-xs-1">
                                      <input type="number" name="qty[]" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                      <input type="number" name="balance[]" readonly>
                                    </div>
                                    <div class="col-xs-1">
                                      <input type="number" name="qty_balance[]" readonly>
                                    </div>
                                    <div class="col-xs-2">
                                      <input type="text" name="tanggal[]" class="datepicker" required>
                                    </div>
                                    <div class="col-xs-1">
                                      <button type="button" name="btn_add_progres" class="btn btn-xs btn-flat btn-success">
                                        <i class="fa fa-plus"></i>
                                      </button>
                                    </div>
                                  </div>
                                <?php }
                              ?>
                            </td>
                          </tr>
                          <?php $no = $no + 1; ?>
                        <?php }
                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
          <div class="box-footer">
            <div class="row pull-right">
              <div class="col-xs-12">
                <button id="<?php echo $data['id']; ?>" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.sewing.progres.js'); ?>"></script>
</body>
</html>