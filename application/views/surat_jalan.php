<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | Surat Jalan</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Surat Jalan</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="table" class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row" style="padding-bottom: 10px;">
                <div class="col-xs-12">
                  <button name="btn_add" class="btn btn-xs btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nomor</th>
                      <th>Tanggal</th>
                      <th>Jumlah Koli</th>
                      <th>Harga</th>
                      <th>Keterangan</th>
                      <th style="min-width: 100px;">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="form" class="row" style="display: none;">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 id="formTitle">Tambah Data</h3>
            </div>
            <div class="box-body">
              <form id="formData">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="form-group col-md-3">
                  <label>Nomor</label>
                  <input type="text" name="nomor" class="form-control" placeholder="Nomor" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Tanggal</label>
                  <input type="text" name="tanggal" class="form-control datepicker" placeholder="Tanggal" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Jumlah Koli</label>
                  <input type="number" name="jumlah_koli" class="form-control" placeholder="Jumlah Koli">
                </div>
                <div class="form-group col-md-3">
                  <label>Harga</label>
                  <input type="number" name="harga" class="form-control" placeholder="Harga">
                </div>
                <div class="form-group col-md-12">
                  <label>Keterangan</label>
                  <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                </div>
                
                <div class="form-group col-md-12 list-detail">
                  <label><h3>Detail</h3></label>
                  <div class="row baris">
                    <div class="col-md-1">
                      <label>Tambah</label>
                      <button name="btn_add_detail" class="btn btn-success btn-flat btn-block"><i class="fa fa-plus"></i></button>
                    </div>
                    <div class="form-group col-md-2">
                      <label>No. Order</label>
                      <input type="text" name="no_order[]" class="form-control" placeholder="No. Order" required>
                    </div>
                    <div class="form-group col-md-2">
                      <label>Nama_barang</label>
                      <input type="text" name="nama_barang[]" class="form-control" placeholder="Nama Barang" required>
                    </div>
                    <div class="col-md-7">
                      <div class="row">
                        <div class="form-group col-md-2">
                          <label>Ukuran S</label>
                          <input type="number" name="ukuran_s[]" class="form-control" placeholder="Ukuran S" required>
                        </div>
                        <div class="form-group col-md-2">
                          <label>Ukuran M</label>
                          <input type="number" name="ukuran_m[]" class="form-control" placeholder="Ukuran M" required>
                        </div>
                        <div class="form-group col-md-2">
                          <label>Ukuran L</label>
                          <input type="number" name="ukuran_l[]" class="form-control" placeholder="Ukuran L" required>
                        </div>
                        <div class="form-group col-md-2">
                          <label>Ukuran XL</label>
                          <input type="number" name="ukuran_xl[]" class="form-control" placeholder="Ukuran XL" required>
                        </div>
                        <div class="form-group col-md-2">
                          <label>Ukuran XXL</label>
                          <input type="number" name="ukuran_xxl[]" class="form-control" placeholder="Ukuran XXL" required>
                        </div>
                        <div class="form-group col-md-2">
                          <label>Jumlah</label>
                          <input type="number" name="jumlah[]" class="form-control" placeholder="Jumlah" required>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-12" style="display:none;">
                      <label>Keterangan</label>
                      <textarea name="keterangan_detail[]" class="form-control" placeholder="Keterangan"></textarea>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="box-footer">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <button id="0" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                  <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <template name="detail">
    <div class="row baris">
      <div class="col-md-1">
        <label>Hapus</label>
        <button name="btn_delete_detail" class="btn btn-danger btn-flat btn-block"><i class="fa fa-trash"></i></button>
      </div>
      <div class="form-group col-md-2">
        <label>No. Order</label>
        <input type="text" name="no_order[]" class="form-control" placeholder="No. Order" required>
      </div>
      <div class="form-group col-md-2">
        <label>Nama_barang</label>
        <input type="text" name="nama_barang[]" class="form-control" placeholder="Nama Barang" required>
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="form-group col-md-2">
            <label>Ukuran S</label>
            <input type="number" name="ukuran_s[]" class="form-control" placeholder="Ukuran S" value="0" required>
          </div>
          <div class="form-group col-md-2">
            <label>Ukuran M</label>
            <input type="number" name="ukuran_m[]" class="form-control" placeholder="Ukuran M" value="0" required>
          </div>
          <div class="form-group col-md-2">
            <label>Ukuran L</label>
            <input type="number" name="ukuran_l[]" class="form-control" placeholder="Ukuran L" value="0" required>
          </div>
          <div class="form-group col-md-2">
            <label>Ukuran XL</label>
            <input type="number" name="ukuran_xl[]" class="form-control" placeholder="Ukuran XL" value="0" required>
          </div>
          <div class="form-group col-md-2">
            <label>Ukuran XXL</label>
            <input type="number" name="ukuran_xxl[]" class="form-control" placeholder="Ukuran XXL" value="0" required>
          </div>
          <div class="form-group col-md-2">
            <label>Jumlah</label>
            <input type="number" name="jumlah[]" class="form-control" placeholder="Jumlah" value="0" required>
          </div>
        </div>
      </div>
      <div class="form-group col-md-12" style="display:none;">
        <label>Keterangan</label>
        <textarea name="keterangan_detail[]" class="form-control" placeholder="Keterangan"></textarea>
      </div>
    </div>
  </template>

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.surat.jalan.js'); ?>"></script>
</body>
</html>