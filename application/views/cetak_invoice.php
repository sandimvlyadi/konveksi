<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Invoice</title>
</head>
<body onload="print()">
    <table width="100%">
      <tr>
        <td>
          <img src="<?php echo base_url('assets/dist/img/agp.png'); ?>" alt="AGP" style="width:300px;margin-top:-25px;">
        </td>
        <td>
          <table>
            <tr>
              <td align="right"><b>PO. No</b></td>
              <td>:</td>
              <td style="width:200px;"><?php echo $data->nomor; ?></td>
            </tr>
            <tr>
              <td align="right"><b>INVOICE NO.</b></td>
              <td>:</td>
              <td><?php echo '#' . str_pad($data->id, 6, '0', STR_PAD_LEFT) ?></td>
            </tr>
            <tr>
              <td align="right"><b>ORDER DATE</b></td>
              <td>:</td>
              <td><?php echo date('d F Y', strtotime($data->tanggal)); ?></td>
            </tr>
            <tr>
              <td align="right"><b>PAYMENT</b></td>
              <td>:</td>
              <td><?php echo $data->nama_payment; ?></td>
            </tr>
            <tr>
              <td valign="top" align="right"><b>Bill To</b></td>
              <td valign="top">:</td>
              <td style="word-wrap:break-word;">
                <?php echo $data->nama_customer; ?> <br>
                <?php echo $data->alamat; ?> <br>
                <?php echo $data->kontak; ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <hr color="black" background-color="black" size="3">
    <table width="100%">
      <thead>
        <tr>
          <th>ITEM DESCRIPTION</th>
          <th>S</th>
          <th>M</th>
          <th>L</th>
          <th>XL</th>
          <th>XXL</th>
          <th>QTY</th>
          <th colspan="2">PRICE</th>
          <th colspan="2">ITEM TOTAL</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($detail as $key => $value) { ?>
            <tr>
              <td><?php echo $data->nama_item . ' ' . $value->nama_warna; ?></td>
              <td align="center"><?php if(intval($value->ukuran_s)>0) echo $value->ukuran_s; ?></td>
              <td align="center"><?php if(intval($value->ukuran_m)>0) echo $value->ukuran_m; ?></td>
              <td align="center"><?php if(intval($value->ukuran_l)>0) echo $value->ukuran_l; ?></td>
              <td align="center"><?php if(intval($value->ukuran_xl)>0) echo $value->ukuran_xl; ?></td>
              <td align="center"><?php if(intval($value->ukuran_xxl)>0) echo $value->ukuran_xxl; ?></td>
              <td align="center"><?php if(intval($value->qty)>0) echo $value->qty; ?></td>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rp.</td>
              <td align="right"><?php if(intval($value->harga)>0) echo number_format($value->harga,2,',','.'); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td>Rp.</td>
              <td align="right"><?php if(intval($value->total)>0) echo number_format($value->total,2,',','.'); ?></td>
            </tr>
          <?php }
        ?>
        <tr height="50">
          <td colspan="11"></td>
        </tr>
        <tr style="font-weight:bold;">
          <td colspan="8"></td>
          <td align="right" style="border-bottom:1px solid black;">Subtotal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="border-bottom:1px solid black;">Rp.</td>
          <td align="right" style="border-bottom:1px solid black;"><?php if(intval($data->total)>0) echo number_format($data->total,2,',','.'); ?></td>
        </tr>
        <tr style="font-weight:bold;">
          <td colspan="8"></td>
          <td align="right" style="border-bottom:1px solid black;">DownPayment &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td style="border-bottom:1px solid black;">Rp.</td>
          <td align="right" style="border-bottom:1px solid black;"><?php if(intval($data->dp)>0) echo number_format($data->dp,2,',','.'); ?></td>
        </tr>
        <tr style="font-weight:bold;">
          <td colspan="8"></td>
          <td align="right"></td>
          <td>Rp.</td>
          <td align="right"><?php if(intval($data->pelunasan)>0) echo number_format($data->pelunasan,2,',','.'); ?></td>
        </tr>
      </tbody>
    </table>
    <hr color="black" background-color="black" size="3">
    <table width="75%" style="font-weight:bold;margin-top:25px;">
      <tr>
        <td align="center">TRANSFER BANK</td>
        <td></td>
      </tr>
      <tr>
        <td align="right" style="border-right:1px solid black;">
          BCA 2781467776 <br>
          BRI 1752 01 002841 501 <br>
          MANDIRI 1320010570894 <br>
        </td>
        <td>
          &nbsp;&nbsp;&nbsp; a.n Asep Mochdar
        </td>
      </tr>
    </table>
</body>
</html>