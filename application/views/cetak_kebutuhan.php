<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Kebutuhan</title>
</head>
<body onload="print()">
    <table width="100%">
      <tr>
        <td>
          <img src="<?php echo base_url('assets/dist/img/agp.png'); ?>" alt="AGP" style="width:300px;margin-top:-25px;">
        </td>
        <td>
          <table>
            <tr>
              <td align="right"><b>PO. No</b></td>
              <td>:</td>
              <td style="width:200px;"><?php echo $data->nomor; ?></td>
            </tr>
            <tr>
              <td align="right"><b>INVOICE NO.</b></td>
              <td>:</td>
              <td><?php echo $data->nomor_invoice ?></td>
            </tr>
            <tr>
              <td align="right"><b>ORDER DATE</b></td>
              <td>:</td>
              <td><?php echo date('d F Y', strtotime($data->tanggal)); ?></td>
            </tr>
            <!-- <tr>
              <td align="right"><b>PAYMENT</b></td>
              <td>:</td>
              <td><?php echo $data->nama_payment; ?></td>
            </tr> -->
            <tr>
              <td valign="top" align="right"><b>Bill To</b></td>
              <td valign="top">:</td>
              <td style="word-wrap:break-word;">
                <?php echo $data->nama_customer; ?> <br>
                <?php echo $data->alamat; ?> <br>
                <?php echo $data->kontak; ?>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <hr color="black" background-color="black" size="3">
    <table width="100%">
      <thead>
        <tr>
          <th>Bahan Baku</th>
          <th>Qty</th>
          <th>Satuan</th>
          <th>Harga</th>
          <th>Cek List</th>
        </tr>
      </thead>
      <tbody>
        <?php
          foreach ($detail as $key => $value) { ?>
            <tr>
              <td><?php echo $value->nama_bahan_baku; ?></td>
              <td align="center"><?php echo $value->qty; ?></td>
              <td align="center"><?php echo $value->nama_satuan; ?></td>
              <td align="center"><?php echo $value->harga; ?></td>
              <td align="center">
                <?php
                if($value->status == 1){ ?>
                  Sudah
                <?php } else{ ?>
                  Belum
                <?php }
                ?>
              </td>
            </tr>
          <?php }
          
        ?>
      </tbody>
    </table>
    <hr color="black" background-color="black" size="3">
    <table width="75%" style="font-weight:bold;margin-top:25px;">
      <tr>
        <td align="center">TRANSFER BANK</td>
        <td></td>
      </tr>
      <tr>
        <td align="right" style="border-right:1px solid black;">
          BCA 2781467776 <br>
          BRI 1752 01 002841 501 <br>
          MANDIRI 1320010570894 <br>
        </td>
        <td>
          &nbsp;&nbsp;&nbsp; a.n Asep Mochdar
        </td>
      </tr>
    </table>
</body>
</html>