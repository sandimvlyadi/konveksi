<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | Delivery</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Delivery</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="table" class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row" style="padding-bottom: 10px;">
                <div class="col-xs-12">
                  <button name="btn_add" class="btn btn-xs btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nomor PO</th>
                      <th>Jumlah</th>
                      <th>Jumlah Selesai</th>
                      <th>Target</th>
                      <th>Keterangan</th>
                      <th style="min-width: 100px;">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="form" class="row" style="display: none;">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 id="formTitle">Tambah Data</h3>
            </div>
            <div class="box-body">
              <form id="formData">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="form-group col-md-6">
                  <label>Nomor PO</label>
                  <select name="id_t_po" class="form-control" style="width: 100%;"></select>
                </div>
                <div class="form-group col-md-6">
                  <label>Target</label>
                  <input type="text" name="target" class="form-control datepicker" placeholder="Target" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Keterangan</label>
                  <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                </div>
                <div class="form-group col-md-12 list-biaya">
                  <label><h3>Biaya</h3></label>
                  <div class="row">
                    <div class="col-md-1">
                      <label>Tambah</label>
                      <button name="btn_add_biaya" class="btn btn-success btn-flat btn-block"><i class="fa fa-plus"></i></button>
                    </div>
                    <div class="form-group col-md-3">
                      <label>Nama</label>
                      <select name="id_m_biaya[]" class="form-control" style="width: 100%;">
                        <option value="0">- Pilih Biaya -</option>
                        <?php
                        foreach ($biaya as $key => $value) { ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_biaya']; ?></option>
                        <?php }
                        ?>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label>Biaya</label>
                      <input type="number" name="biaya_delivery[]" class="form-control" placeholder="Biaya" required>
                    </div>
                    <div class="form-group col-md-5">
                      <label>Keterangan</label>
                      <textarea name="keterangan_biaya[]" class="form-control" placeholder="Keterangan"></textarea>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="box-footer">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <button id="0" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                  <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <template name="biaya">
    <div class="row">
      <div class="col-md-1">
        <label>Hapus</label>
        <button name="btn_delete_biaya" class="btn btn-danger btn-flat btn-block"><i class="fa fa-trash"></i></button>
      </div>
      <div class="form-group col-md-3">
        <label>Nama</label>
        <select name="id_m_biaya[]" class="form-control" style="width: 100%;">
          <option value="0">- Pilih biaya -</option>
          <?php
          foreach ($biaya as $key => $value) { ?>
            <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_biaya']; ?></option>
          <?php }
          ?>
        </select>
      </div>
      <div class="form-group col-md-3">
        <label>Biaya</label>
        <input type="number" name="biaya_delivery[]" class="form-control" placeholder="Biaya" required>
      </div>
      <div class="form-group col-md-5">
        <label>Keterangan</label>
        <textarea name="keterangan_biaya[]" class="form-control" placeholder="Keterangan"></textarea>
      </div>
    </div>
  </template>

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.delivery.js'); ?>"></script>
</body>
</html>