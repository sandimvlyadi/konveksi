<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admina | Belanja Terima</title>
  <?php $this->load->view('script-head'); ?>
</head>
<body class="hold-transition skin-blue">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('header'); ?>

  <!-- =============================================== -->

  <?php $this->load->view('sidebar'); ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Belanja Terima</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div id="table" class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="row" style="padding-bottom: 10px;">
                <div class="col-xs-6">
                  Nomor PO: <strong><?php echo $po['nomor']; ?></strong>
                </div>
                <div class="col-xs-6">
                  <button name="btn_add" class="btn btn-xs btn-primary btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
                  <a href="<?php echo base_url('belanja/'); ?>" class="btn btn-xs btn-danger btn-flat pull-right" style="margin-right:10px;"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Bahan Baku</th>
                      <th>Qty</th>
                      <th>Tanggal</th>
                      <th>Keterangan</th>
                      <th style="min-width: 75px;">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="form" class="row" style="display: none;">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 id="formTitle">Tambah Data</h3>
            </div>
            <div class="box-body">
              <form id="formData">
                <input id="csrf" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="" />
                <div class="form-group col-md-3">
                  <label>Nomor PO</label>
                  <input type="hidden" name="id_t_po" value="<?php echo $po['id']; ?>" required>
                  <input type="hidden" name="id_t_belanja" value="<?php echo $id_t_belanja; ?>" required>
                  <input type="text" name="nomor" class="form-control" placeholder="Nomor PO" value="<?php echo $po['nomor']; ?>" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label>Bahan Baku</label>
                  <select name="id_m_bahan_baku" class="form-control" style="width: 100%;">
                    <option value="0">- Pilih Bahan Baku -</option>
                    <?php
                    foreach ($bahan_baku as $key => $value) { ?>
                        <option value="<?php echo $value['id']; ?>"><?php echo $value['nama_bahan_baku']; ?></option>
                    <?php }
                    ?>
                  </select>
                </div>
                <div class="form-group col-md-3">
                  <label>Qty</label>
                  <input type="number" name="qty" class="form-control" placeholder="Qty" required>
                </div>
                <div class="form-group col-md-3">
                  <label>Tanggal</label>
                  <input type="text" name="tanggal" class="form-control datepicker" placeholder="Tanggal" required>
                </div>                
                <div class="form-group col-md-12">
                  <label>Keterangan</label>
                  <textarea name="keterangan" class="form-control" placeholder="Keterangan"></textarea>
                </div>
              </form>
            </div>
            <div class="box-footer">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <button id="0" name="btn_save" class="btn btn-xs btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
                  <button name="btn_cancel" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i> Batal</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('footer'); ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('script-foot'); ?>
<script src="<?php echo base_url('assets/admina/js/admina.belanja.terima.js'); ?>"></script>
</body>
</html>