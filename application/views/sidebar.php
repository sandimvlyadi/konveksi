<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="li-dashboard"><a href="<?php echo base_url('dashboard/'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li class="treeview li-po">
        <a href="#">
          <i class="fa fa-dollar"></i> <span>Purchase Order</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="li-t-po"><a href="<?php echo base_url('purchase-order/'); ?>"><i class="fa fa-circle-o"></i> List PO</a></li>
          <li class="li-t-kebutuhan"><a href="<?php echo base_url('kebutuhan/'); ?>"><i class="fa fa-circle-o"></i> List Kebutuhan</a></li>
          <li class="li-belanja"><a href="<?php echo base_url('belanja/'); ?>"><i class="fa fa-circle-o"></i> <span>Purchasing</span></a></li>
          <li class="li-biaya-bulanan"><a href="<?php echo base_url('biaya-bulanan/'); ?>"><i class="fa fa-circle-o"></i> Biaya Bulanan</a></li>
        </ul>
      </li>
      <li class="li-cutting"><a href="<?php echo base_url('cutting/'); ?>"><i class="fa fa-scissors"></i> <span>Cutting</span></a></li>
      <li class="li-naik-jahit"><a href="<?php echo base_url('naik-jahit/'); ?>"><i class="fa fa-random"></i> <span>Sewing</span></a></li>
      <li class="li-qc"><a href="<?php echo base_url('qc/'); ?>"><i class="fa fa-check-square-o"></i> <span>Quality Control</span></a></li>
      <li class="li-packing"><a href="<?php echo base_url('packing/'); ?>"><i class="fa fa-cube"></i> <span>Packing</span></a></li>
      <li class="li-delivery"><a href="<?php echo base_url('delivery/'); ?>"><i class="fa fa-truck"></i> <span>Delivery</span></a></li>
      <li class="treeview li-gudang">
        <a href="#">
          <i class="fa fa-industry"></i> <span>Gudang</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="li-gudang-bahan"><a href="<?php echo base_url('gudang-bahan/'); ?>"><i class="fa fa-circle-o"></i> Bahan</a></li>
          <li class="li-gudang-barang-jadi"><a href="<?php echo base_url('gudang-barang-jadi/'); ?>"><i class="fa fa-circle-o"></i> Barang Jadi</a></li>
        </ul>
      </li>
      <li class="li-surat-jalan"><a href="<?php echo base_url('surat-jalan/'); ?>"><i class="fa fa-file-text-o"></i> <span>Surat Jalan</span></a></li>
      <li class="treeview li-master">
        <a href="#">
          <i class="fa fa-book"></i> <span>Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="li-master-customer"><a href="<?php echo base_url('master-customer/'); ?>"><i class="fa fa-circle-o"></i> Customer</a></li>
          <li class="li-master-penjahit"><a href="<?php echo base_url('master-penjahit/'); ?>"><i class="fa fa-circle-o"></i> Penjahit</a></li>
          <li class="li-master-bahan-baku"><a href="<?php echo base_url('master-bahan-baku/'); ?>"><i class="fa fa-circle-o"></i> Bahan Baku</a></li>
          <li class="li-master-warna"><a href="<?php echo base_url('master-warna/'); ?>"><i class="fa fa-circle-o"></i> Warna</a></li>
          <li class="li-master-payment"><a href="<?php echo base_url('master-payment/'); ?>"><i class="fa fa-circle-o"></i> Payment</a></li>
          <li class="li-master-biaya"><a href="<?php echo base_url('master-biaya/'); ?>"><i class="fa fa-circle-o"></i> Biaya</a></li>
          <li class="li-master-satuan"><a href="<?php echo base_url('master-satuan/'); ?>"><i class="fa fa-circle-o"></i> Satuan</a></li>
          <li class="li-master-pengguna"><a href="<?php echo base_url('master-pengguna/'); ?>"><i class="fa fa-circle-o"></i> Pengguna</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
