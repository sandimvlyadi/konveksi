<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;

class Download extends CI_Controller {

	private $userData;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model', 'login');
		$this->load->model('cetak_model', 'model');

		$this->userData = array(
			'session'	=> $this->session->userdata('userSession'),
			'host'		=> $this->input->get_request_header('Host', TRUE),
			'referer'	=> $this->input->get_request_header('Referer', TRUE),
			'agent'		=> $this->input->get_request_header('User-Agent', TRUE),
			'ipaddr'	=> $this->input->ip_address()
		);

		$auth = $this->login->auth($this->userData);
		if(!$auth['result']){
			redirect('login/');
		}
	}

	public function index()
	{
		redirect('dashboard/');
	}

	public function purchase_order($id = 0)
	{
		$data = $this->model->purchase_order($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'purchase_order_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
        $dompdf = new Dompdf();
        $html = $this->load->view('download_purchase_order', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/purchase-order/'.$filename, $output);

        force_download('./download/purchase-order/'.$filename, NULL);
	}

	public function kebutuhan($id = 0)
	{
		$data = $this->model->kebutuhan($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'kebutuhan_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
        $dompdf = new Dompdf();
        $html = $this->load->view('download_kebutuhan', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/kebutuhan/'.$filename, $output);

        force_download('./download/kebutuhan/'.$filename, NULL);
	}

	public function belanja($id = 0)
	{
		$data = $this->model->belanja($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'belanja_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
        $dompdf = new Dompdf();
        $html = $this->load->view('download_belanja', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/belanja/'.$filename, $output);

        force_download('./download/belanja/'.$filename, NULL);
	}

	public function cutting($id = 0)
	{
		$data = $this->model->cutting($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'cutting_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
		$dompdf = new Dompdf();
		$dompdf->set_paper('a4', 'landscape');
        $html = $this->load->view('download_cutting', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/cutting/'.$filename, $output);

        force_download('./download/cutting/'.$filename, NULL);
	}

	public function naik_jahit($id = 0)
	{
		$data = $this->model->naik_jahit($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'sewing_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
		$dompdf = new Dompdf();
		$dompdf->set_paper('a4', 'landscape');
        $html = $this->load->view('download_naik_jahit', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/sewing/'.$filename, $output);

        force_download('./download/sewing/'.$filename, NULL);
	}

	public function qc($id = 0)
	{
		$data = $this->model->qc($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'qc_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
		$dompdf = new Dompdf();
		$dompdf->set_paper('a4', 'landscape');
        $html = $this->load->view('download_qc', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/qc/'.$filename, $output);

        force_download('./download/qc/'.$filename, NULL);
	}

	public function packing($id = 0)
	{
		$data = $this->model->packing($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

        $this->load->library('pdf');
        $this->load->helper('download');
        $filename = 'packing_'. str_replace('/', '_', $data['data']->nomor) .'.pdf';
		$dompdf = new Dompdf();
		$dompdf->set_paper('a4', 'landscape');
        $html = $this->load->view('download_packing', $data, TRUE);
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents('./download/packing/'.$filename, $output);

        force_download('./download/packing/'.$filename, NULL);
	}

}
