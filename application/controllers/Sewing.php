<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing extends CI_Controller {

	private $userData;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model', 'login');
		$this->load->model('sewing_model', 'model');
		$this->load->model('cutting_model');
		$this->load->model('purchase_order_model');

		$this->userData = array(
			'session'	=> $this->session->userdata('userSession'),
			'host'		=> $this->input->get_request_header('Host', TRUE),
			'referer'	=> $this->input->get_request_header('Referer', TRUE),
			'agent'		=> $this->input->get_request_header('User-Agent', TRUE),
			'ipaddr'	=> $this->input->ip_address()
		);

		$auth = $this->login->auth($this->userData);
		if(!$auth['result']){
			redirect('login/');
		}
	}

	public function index()
	{
        $this->load->view('sewing');
	}

	public function progres($id = 0)
	{
		$data = $this->purchase_order_model->select($id);
		if (!$data['result']) {
			redirect('sewing/');
		}

		$data = array(
			'data'		=> $data['data'],
			'detail'	=> $this->purchase_order_model->detail($id)
		);

		for ($i=0; $i < count($data['detail']); $i++) { 
			$data['detail'][$i]->progres = $this->model->progres($data['detail'][$i]);
		}

        $this->load->view('sewing_progres', $data);
	}

	public function datatable()
	{
		$response 	= array(
			'result'	=> false,
			'msg'		=> ''
		);

		$param		= $_GET;
		$response 	= $this->model->datatable($param);
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	public function save()
	{
		$response 	= array(
			'result'	=> false,
			'msg'		=> ''
		);

		$param = array(
			'userData' => $this->userData,
			'postData' => $this->security->xss_clean($_POST)
		);
		$response = $this->model->save($param);

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	public function select_progres($id = 0)
	{
		$response 	= array(
			'result'	=> false,
			'msg'		=> ''
		);

		$response = $this->model->select_progres($id);

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

}
