<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {

	private $userData;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model', 'login');
		$this->load->model('cetak_model', 'model');
		$this->load->model('purchase_order_model');
		$this->load->model('cutting_model');

		$this->userData = array(
			'session'	=> $this->session->userdata('userSession'),
			'host'		=> $this->input->get_request_header('Host', TRUE),
			'referer'	=> $this->input->get_request_header('Referer', TRUE),
			'agent'		=> $this->input->get_request_header('User-Agent', TRUE),
			'ipaddr'	=> $this->input->ip_address()
		);

		$auth = $this->login->auth($this->userData);
		if(!$auth['result']){
			redirect('login/');
		}
	}

	public function index()
	{
		redirect('dashboard/');
	}

	public function purchase_order($id = 0)
	{
		$data = $this->model->purchase_order($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_purchase_order', $data);
	}

	public function invoice($id = 0)
	{
		$data = $this->model->invoice($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_invoice', $data);
	}

	public function surat_jalan($id = 0)
	{
		$data = $this->model->surat_jalan($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_surat_jalan', $data);
	}

	public function kebutuhan($id = 0)
	{
		$data = $this->model->kebutuhan($id);

		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_kebutuhan', $data);
	}

	public function naik_jahit($id = 0)
	{
		$data = $this->model->naik_jahit($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_naik_jahit', $data);
	}

	public function qc($id = 0)
	{
		$data = $this->model->qc($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_qc', $data);
	}

	public function packing($id = 0)
	{
		$data = $this->model->packing($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_packing', $data);
	}

	public function belanja($id = 0)
	{
		$data = $this->model->belanja($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_belanja', $data);
	}

	public function biaya_bulanan($id = 0)
	{
		$data = $this->model->biaya_bulanan($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_biaya_bulanan', $data);
	}

	public function cutting($id = 0)
	{
		$data = $this->model->cutting($id);
		
		if (!$data['result']) {
			redirect('dashboard/');
		}

		$this->load->view('cetak_cutting', $data);
	}

	public function sewing($id = 0)
	{
		$data = $this->purchase_order_model->select($id);
		if (!$data['result']) {
			redirect('sewing/');
		}

		$data = array(
			'data'		=> $data['data'],
			'detail'	=> $this->purchase_order_model->detail($id)
		);

		for ($i=0; $i < count($data['detail']); $i++) { 
			$data['detail'][$i]->progres = $this->model->progres($data['detail'][$i]);
		}

        $this->load->view('cetak_sewing', $data);
	}

}
