<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kebutuhan_model extends CI_Model {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model', 'setting');
	}

    function _get($data = array())
    {
        $q = "SELECT a.* FROM `t_purchase_order` a ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`nomor` LIKE '%". $s ."%' OR a.`total` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data kebutuhan untuk po ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*
                FROM
                    `t_purchase_order` a
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $this->db->select('t_purchase_order_detail.*, m_warna.nama_warna');
            $this->db->where('id_t_purchase_order', $id);
            $this->db->join('m_warna', 't_purchase_order_detail.id_m_warna = m_warna.id', 'left');
            $detail = $this->db->get('t_purchase_order_detail');

            $this->db->where('id_t_purchase_order', $id);
            $bahan_baku = $this->db->get('t_purchase_order_kebutuhan');

            $result['result'] = true;
            $result['data'] = $r[0];
            $result['detail'] = $detail->result();
            $result['bahan_baku'] = $bahan_baku->result();
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => true,
            'msg'       => 'Data berhasil disimpan.'
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        $this->db->where('id_t_purchase_order', $id);
        $this->db->where_not_in('id_m_bahan_baku', $f['id_m_bahan_baku']);
        $this->db->delete('t_purchase_order_kebutuhan');

        for ($i=0; $i < count($f['id_m_bahan_baku']); $i++) { 
            if (intval($f['id_m_bahan_baku'][$i]) == 0) {
                $newBahanBaku = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'nama_bahan_baku' => $f['id_m_bahan_baku'][$i]
                );
                $f['id_m_bahan_baku'][$i] = $this->setting->store_bahan_baku($newBahanBaku);
            }

            if (intval($f['id_m_satuan'][$i]) == 0) {
                $newSatuan = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'nama_satuan' => $f['id_m_satuan'][$i]
                );
                $f['id_m_satuan'][$i] = $this->setting->store_satuan($newSatuan);
            }

            $id_bahan_baku = $f['id_m_bahan_baku'][$i];
            $qty = $f['qty'][$i];
            $satuan = $f['id_m_satuan'][$i];
            $harga = $f['harga'][$i];
            $jumlah = intval($qty) * intval($harga);
            $status = $f['status'][$i];
            $keterangan = $f['keterangan_bahan_baku'][$i];

            $this->db->where('id_t_purchase_order', $id);
            $this->db->where('id_m_bahan_baku', $id_bahan_baku);
            $get = $this->db->get('t_purchase_order_kebutuhan');
            if ($get->num_rows() > 0) {
                $update = array(
                    'modified_at' => date('Y-m-d H:i:s'),
                    'qty' => $qty,
                    'id_m_satuan' => $satuan,
                    'harga' => $harga,
                    'jumlah' => $jumlah,
                    'status' => $status,
                    'keterangan' => $keterangan
                );
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_bahan_baku', $id_bahan_baku);
                $this->db->update('t_purchase_order_kebutuhan', $update);

                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_bahan_baku', $id_bahan_baku);
                $get = $this->db->get('t_purchase_order_purchasing');
                if ($get->num_rows() > 0) {
                    $update = array(
                        'modified_at' => date('Y-m-d H:i:s'),
                        'id_m_satuan' => $satuan
                    );
                    $this->db->where('id_t_purchase_order', $id);
                    $this->db->where('id_m_bahan_baku', $id_bahan_baku);
                    $this->db->update('t_purchase_order_purchasing', $update);
                } else{
                    $insert = array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'id_t_purchase_order' => $id,
                        'id_m_bahan_baku' => $id_bahan_baku,
                        'qty' => '0',
                        'id_m_satuan' => $satuan,
                        'harga' => '0',
                        'jumlah' => '0',
                        'status' => '0',
                    );
                    $this->db->insert('t_purchase_order_purchasing', $insert);
                }
            } else{
                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_purchase_order' => $id,
                    'id_m_bahan_baku' => $id_bahan_baku,
                    'qty' => $qty,
                    'id_m_satuan' => $satuan,
                    'harga' => $harga,
                    'jumlah' => $jumlah,
                    'status' => $status,
                    'keterangan' => $keterangan
                );
                $this->db->insert('t_purchase_order_kebutuhan', $insert);

                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_purchase_order' => $id,
                    'id_m_bahan_baku' => $id_bahan_baku,
                    'qty' => '0',
                    'id_m_satuan' => $satuan,
                    'harga' => '0',
                    'jumlah' => '0',
                    'status' => '0',
                );
                $this->db->insert('t_purchase_order_purchasing', $insert);
            }
        }

        return $result;
    }

    // function delete($data = array())
    // {
    //     $result = array(
    //         'result'    => false,
    //         'msg'       => ''
    //     );

    //     $u = $data['userData'];
    //     $d = $data['postData'];
    //     $id = $d['id'];
    //     $q = "UPDATE `t_purchase_order` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
    //     if ($this->db->simple_query($q)) {
    //         $result['result'] = true;
    //         $result['msg'] = 'Data berhasil dihapus.';
    //     } else{
    //         $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
    //     }

    //     return $result;
    // }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_purchase_order_kebutuhan` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_purchase_order_kebutuhan` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
