<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.*, b.`nomor`, b.`jumlah`, SUM(c.`ukuran_s`) AS `ukuran_s`, SUM(c.`ukuran_m`) AS `ukuran_m`, SUM(c.`ukuran_l`) AS `ukuran_l`, SUM(c.`ukuran_xl`) AS `ukuran_xl`, SUM(c.`ukuran_xxl`) AS `ukuran_xxl` 
            FROM `t_delivery` a 
            LEFT JOIN `t_purchase_order` b ON a.`id_t_po` = b.`id` 
            LEFT JOIN `t_delivery_progres` c ON a.`id` = c.`id_t_delivery` ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (b.`nomor` LIKE '%". $s ."%' OR b.`jumlah` LIKE '%". $s ."%' OR a.`target` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL AND c.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL AND c.`deleted_at` IS NULL ";
        }

        $q .= "GROUP BY a.`id` ";

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'nomor' || $col == 'jumlah') {
                    $q .= "ORDER BY b.`". $col ."` ". $dir ." ";
                } elseif ($col == 'jumlah_selesai') {
                    # code...
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _get_progres($data = array())
    {
        $q = "SELECT a.* FROM `t_delivery_progres` a ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`ukuran_s` LIKE '%". $s ."%' OR a.`ukuran_m` LIKE '%". $s ."%' OR a.`ukuran_l` LIKE '%". $s ."%' OR a.`ukuran_xl` LIKE '%". $s ."%' OR a.`ukuran_xxl` LIKE '%". $s ."%' OR a.`tanggal` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL AND 
                a.`id_t_delivery` = '". $this->db->escape_str($data['id_t_delivery']) ."' ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL AND a.`id_t_delivery` = '". $this->db->escape_str($data['id_t_delivery']) ."' ";
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _list_progres($data = array())
    {
        $q = $this->_get_progres($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _filtered_progres($data = array())
    {
        $q = $this->_get_progres($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function _all_progres($data = array())
    {
        $data['all'] = true;
        $q = $this->_get_progres($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function datatable_progres($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list_progres($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all_progres($data),
                'recordsFiltered'   => $this->_filtered_progres($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data delivery untuk po ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*,
                    b.`nomor`
                FROM
                    `t_delivery` a
                LEFT JOIN
                    `t_purchase_order` b
                        ON
                    a.`id_t_po` = b.`id`
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $this->db->where('id_t_delivery', $id);
            $biaya = $this->db->get('t_delivery_biaya');

            $result['result'] = true;
            $result['data'] = $r[0];
            $result['biaya'] = $biaya->result();
        }

        return $result;
    }

    function edit_progres($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data progres untuk po ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*
                FROM
                    `t_delivery_progres` a
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r[0];
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        if ($id == 0) {
            $insert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'id_t_po' => $f['id_t_po'],
                'target' => $f['target'],
                'keterangan' => $f['keterangan']
            );
            $this->db->insert('t_delivery', $insert);
            $id = $this->db->insert_id();

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            for ($i=0; $i < count($f['id_m_biaya']); $i++) { 
                $id_biaya = $f['id_m_biaya'][$i];
                $biaya = $f['biaya_delivery'][$i];
                $keterangan = $f['keterangan_biaya'][$i];

                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_delivery' => $id,
                    'id_m_biaya' => $id_biaya,
                    'biaya_delivery' => $biaya,
                    'keterangan' => $keterangan
                );
                $this->db->insert('t_delivery_biaya', $insert);
            }
        } else{
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'id_t_po' => $f['id_t_po'],
                'target' => $f['target'],
                'keterangan' => $f['keterangan']
            );
            $this->db->where('id', $id);
            $this->db->update('t_delivery', $update);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            $this->db->where('id_t_delivery', $id);
            $this->db->where_not_in('id_m_biaya', $f['id_m_biaya']);
            $this->db->delete('t_delivery_biaya');

            for ($i=0; $i < count($f['id_m_biaya']); $i++) { 
                $id_biaya = $f['id_m_biaya'][$i];
                $biaya = $f['biaya_delivery'][$i];
                $keterangan = $f['keterangan_biaya'][$i];

                $this->db->where('id_t_delivery', $id);
                $this->db->where('id_m_biaya', $id_biaya);
                $get = $this->db->get('t_delivery_biaya');
                if ($get->num_rows() > 0) {
                    $update = array(
                        'modified_at' => date('Y-m-d H:i:s'),
                        'biaya_delivery' => $biaya,
                        'keterangan' => $keterangan
                    );
                    $this->db->where('id_t_delivery', $id);
                    $this->db->where('id_m_biaya', $id_biaya);
                    $this->db->update('t_delivery_biaya', $update);
                } else{
                    $insert = array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'id_t_delivery' => $id,
                        'id_m_biaya' => $id_biaya,
                        'biaya_delivery' => $biaya,
                        'keterangan' => $keterangan
                    );
                    $this->db->insert('t_delivery_biaya', $insert);
                }
            }
        }

        return $result;
    }

    function save_progres($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        if ($id == 0) {
            $insert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'id_t_delivery' => $f['id_t_delivery'],
                'ukuran_s' => $f['ukuran_s'],
                'ukuran_m' => $f['ukuran_m'],
                'ukuran_l' => $f['ukuran_l'],
                'ukuran_xl' => $f['ukuran_xl'],
                'ukuran_xxl' => $f['ukuran_xxl'],
                'tanggal' => $f['tanggal'],
                'keterangan' => $f['keterangan']
            );
            $this->db->insert('t_delivery_progres', $insert);
            $id = $this->db->insert_id();

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';
        } else{
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'id_t_delivery' => $f['id_t_delivery'],
                'ukuran_s' => $f['ukuran_s'],
                'ukuran_m' => $f['ukuran_m'],
                'ukuran_l' => $f['ukuran_l'],
                'ukuran_xl' => $f['ukuran_xl'],
                'ukuran_xxl' => $f['ukuran_xxl'],
                'tanggal' => $f['tanggal'],
                'keterangan' => $f['keterangan']
            );
            $this->db->where('id', $id);
            $this->db->update('t_delivery_progres', $update);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_delivery` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function delete_progres($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_delivery_progres` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_delivery` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_delivery` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

    function select_progres($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_delivery_progres` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_delivery_progres` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
