<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Naik_jahit_model extends CI_Model {

    function _get($data = array())
    {
        $q =    "SELECT 
                    a.*, 
                    (
                        SELECT
                            IFNULL(SUM(`ukuran_s`) + SUM(`ukuran_m`) + SUM(`ukuran_l`) + SUM(`ukuran_xl`) + SUM(`ukuran_xxl`), 0)
                        FROM
                            `t_cutting_progres`
                        WHERE
                            `id_t_purchase_order` = a.`id`
                    ) AS `jumlah`,
                    (
                        SELECT
                            IFNULL(SUM(`ukuran_s`) + SUM(`ukuran_m`) + SUM(`ukuran_l`) + SUM(`ukuran_xl`) + SUM(`ukuran_xxl`), 0)
                        FROM
                            `t_naik_jahit_progres`
                        WHERE
                            `id_t_purchase_order` = a.`id`
                    ) AS `jumlah_selesai` 
                FROM 
                    `t_purchase_order` a 
                ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`nomor` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        // $q .= "GROUP BY a.`id` ";

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'jumlah') {
                    $q .= "ORDER BY `". $col ."` ". $dir ." ";
                } elseif ($col == 'jumlah_selesai') {
                    $q .= "ORDER BY `". $col ."` ". $dir ." ";
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function progres($data = array())
    {
        $this->db->where('id_t_purchase_order', $data->id_t_purchase_order);
        $this->db->where('id_m_warna', $data->id_m_warna);
        $result = $this->db->get('t_naik_jahit_progres')->result();

        return $result;
    }
    
    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        $this->db->where_in('id_t_purchase_order', $id);
        $this->db->where_not_in('id', $f['progres_id']);
        $this->db->delete('t_naik_jahit_progres');

        for ($i=0; $i < count($f['progres_id']); $i++) { 
            if ($f['progres_id'][$i] == 0) {
                $insert = array(
                    'created_at'            => date('Y-m-d H:i:s'),
                    'id_t_purchase_order'   => $id,
                    'id_m_warna'            => $f['id_m_warna'][$i],
                    'ukuran_s'              => $f['ukuran_s'][$i],
                    'ukuran_m'              => $f['ukuran_m'][$i],
                    'ukuran_l'              => $f['ukuran_l'][$i],
                    'ukuran_xl'             => $f['ukuran_xl'][$i],
                    'ukuran_xxl'            => $f['ukuran_xxl'][$i],
                    'tanggal'               => $f['tanggal'][$i]
                );
                $this->db->insert('t_naik_jahit_progres', $insert);
            } else{
                $update = array(
                    'modified_at'           => date('Y-m-d H:i:s'),
                    'id_t_purchase_order'   => $id,
                    'id_m_warna'            => $f['id_m_warna'][$i],
                    'ukuran_s'              => $f['ukuran_s'][$i],
                    'ukuran_m'              => $f['ukuran_m'][$i],
                    'ukuran_l'              => $f['ukuran_l'][$i],
                    'ukuran_xl'             => $f['ukuran_xl'][$i],
                    'ukuran_xxl'            => $f['ukuran_xxl'][$i],
                    'tanggal'               => $f['tanggal'][$i]
                );
                $this->db->where('id', $f['progres_id'][$i]);
                $this->db->update('t_naik_jahit_progres', $update);
            }
        }

        $result['result'] = true;
        $result['msg'] = 'Data berhasil disimpan.';

        return $result;
    }

    function select_progres($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_naik_jahit_progres` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_naik_jahit_progres` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

    private function ukuran_count($ukuran = 'ukuran_s', $id_po = 0, $id_warna = 0)
	{
		$q = "SELECT IFNULL(SUM(`". $ukuran ."`), 0) AS `total` FROM `t_cutting_progres` WHERE `id_t_purchase_order` = '". $id_po ."' AND `id_m_warna` = '". $id_warna ."' AND `deleted_at` IS NULL GROUP BY `id_m_warna`;";
		$r = $this->db->query($q, false)->result_array();
		if (count($r) > 0) {
			return $r[0]['total'];
		} else{
			return 0;
		}
	}

	public function detail($id = 0)
    {
        $this->db->select('t_purchase_order_detail.*, nama_warna');
        $this->db->join('m_warna', 't_purchase_order_detail.id_m_warna = m_warna.id', 'left');
        $this->db->where('id_t_purchase_order', $id);
		$result = $this->db->get('t_purchase_order_detail')->result();
		
		for ($i=0; $i < count($result); $i++) { 
			$s = $this->ukuran_count('ukuran_s', $id, $result[$i]->id_m_warna);
			$m = $this->ukuran_count('ukuran_m', $id, $result[$i]->id_m_warna);
			$l = $this->ukuran_count('ukuran_l', $id, $result[$i]->id_m_warna);
			$xl = $this->ukuran_count('ukuran_xl', $id, $result[$i]->id_m_warna);
			$xxl = $this->ukuran_count('ukuran_xxl', $id, $result[$i]->id_m_warna);
			$qty = $s + $m + $l + $xl + $xxl;

			$result[$i]->ukuran_s = $s;
			$result[$i]->ukuran_m = $m;
			$result[$i]->ukuran_l = $l;
			$result[$i]->ukuran_xl = $xl;
			$result[$i]->ukuran_xxl = $xxl;
			$result[$i]->qty = $qty;
		}

        return $result;
    }

}
