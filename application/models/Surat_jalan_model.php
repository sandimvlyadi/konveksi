<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_jalan_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.* FROM `t_surat_jalan` a ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`nomor` LIKE '%". $s ."%' OR a.`tanggal` LIKE '%". $s ."%' OR a.`jumlah_koli` LIKE '%". $s ."%' OR a.`harga` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data surat jalan ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*
                FROM
                    `t_surat_jalan` a
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $this->db->where('id_t_surat_jalan', $id);
            $detail = $this->db->get('t_surat_jalan_detail');

            $result['result'] = true;
            $result['data'] = $r[0];
            $result['detail'] = $detail->result();
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        if ($id == 0) {
            $insert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'nomor' => $f['nomor'],
                'tanggal' => $f['tanggal'],
                'jumlah_koli' => $f['jumlah_koli'],
                'harga' => $f['harga'],
                'keterangan' => $f['keterangan']
            );
            $this->db->insert('t_surat_jalan', $insert);
            $id = $this->db->insert_id();

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            for ($i=0; $i < count($f['no_order']); $i++) { 
                $no_order = $f['no_order'][$i];
                $nama_barang = $f['nama_barang'][$i];
                $ukuran_s = $f['ukuran_s'][$i];
                $ukuran_m = $f['ukuran_m'][$i];
                $ukuran_l = $f['ukuran_l'][$i];
                $ukuran_xl = $f['ukuran_xl'][$i];
                $ukuran_xxl = $f['ukuran_xxl'][$i];
                $jumlah = $f['jumlah'][$i];
                $keterangan = $f['keterangan_detail'][$i];

                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_surat_jalan' => $id,
                    'no_order' => $no_order,
                    'nama_barang' => $nama_barang,
                    'ukuran_s' => $ukuran_s,
                    'ukuran_m' => $ukuran_m,
                    'ukuran_l' => $ukuran_l,
                    'ukuran_xl' => $ukuran_xl,
                    'ukuran_xxl' => $ukuran_xxl,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan
                );
                $this->db->insert('t_surat_jalan_detail', $insert);
            }
        } else{
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'nomor' => $f['nomor'],
                'tanggal' => $f['tanggal'],
                'jumlah_koli' => $f['jumlah_koli'],
                'harga' => $f['harga'],
                'keterangan' => $f['keterangan']
            );
            $this->db->where('id', $id);
            $this->db->update('t_surat_jalan', $update);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            $this->db->where('id_t_surat_jalan', $id);
            $this->db->delete('t_surat_jalan_detail');

            for ($i=0; $i < count($f['no_order']); $i++) { 
                $no_order = $f['no_order'][$i];
                $nama_barang = $f['nama_barang'][$i];
                $ukuran_s = $f['ukuran_s'][$i];
                $ukuran_m = $f['ukuran_m'][$i];
                $ukuran_l = $f['ukuran_l'][$i];
                $ukuran_xl = $f['ukuran_xl'][$i];
                $ukuran_xxl = $f['ukuran_xxl'][$i];
                $jumlah = $f['jumlah'][$i];
                $keterangan = $f['keterangan_detail'][$i];

                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_surat_jalan' => $id,
                    'no_order' => $no_order,
                    'nama_barang' => $nama_barang,
                    'ukuran_s' => $ukuran_s,
                    'ukuran_m' => $ukuran_m,
                    'ukuran_l' => $ukuran_l,
                    'ukuran_xl' => $ukuran_xl,
                    'ukuran_xxl' => $ukuran_xxl,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan
                );
                $this->db->insert('t_surat_jalan_detail', $insert);
            }
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_surat_jalan` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_surat_jalan` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_surat_jalan` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
