<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sewing_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.*, IFNULL(SUM(b.`ukuran_s`) + SUM(b.`ukuran_m`) + SUM(b.`ukuran_l`) + SUM(b.`ukuran_xl`) + SUM(b.`ukuran_xxl`), 0) AS `jumlah_selesai` FROM `t_purchase_order` a LEFT JOIN `t_sewing_progres` b ON a.`id` = b.`id_t_purchase_order` ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`nomor` LIKE '%". $s ."%' OR a.`jumlah` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        $q .= "GROUP BY a.`id` ";

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'jumlah_selesai') {
                    $q .= "ORDER BY `". $col ."` ". $dir ." ";
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function select_progres($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_sewing_progres` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_sewing_progres` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

    function progres($data = array())
    {
        $this->db->where('id_t_purchase_order', $data->id_t_purchase_order);
        $this->db->where('id_m_warna', $data->id_m_warna);
        $result = $this->db->get('t_sewing_progres')->result();

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        $this->db->where_in('id_t_purchase_order', $id);
        $this->db->where_not_in('id', $f['progres_id']);
        $this->db->delete('t_sewing_progres');

        for ($i=0; $i < count($f['progres_id']); $i++) { 
            if ($f['progres_id'][$i] == 0) {
                $insert = array(
                    'created_at'            => date('Y-m-d H:i:s'),
                    'id_t_purchase_order'   => $id,
                    'id_m_warna'            => $f['id_m_warna'][$i],
                    'ukuran_s'              => $f['ukuran_s'][$i],
                    'ukuran_m'              => $f['ukuran_m'][$i],
                    'ukuran_l'              => $f['ukuran_l'][$i],
                    'ukuran_xl'             => $f['ukuran_xl'][$i],
                    'ukuran_xxl'            => $f['ukuran_xxl'][$i],
                    'tanggal'               => $f['tanggal'][$i]
                );
                $this->db->insert('t_sewing_progres', $insert);
            } else{
                $update = array(
                    'modified_at'           => date('Y-m-d H:i:s'),
                    'id_t_purchase_order'   => $id,
                    'id_m_warna'            => $f['id_m_warna'][$i],
                    'ukuran_s'              => $f['ukuran_s'][$i],
                    'ukuran_m'              => $f['ukuran_m'][$i],
                    'ukuran_l'              => $f['ukuran_l'][$i],
                    'ukuran_xl'             => $f['ukuran_xl'][$i],
                    'ukuran_xxl'            => $f['ukuran_xxl'][$i],
                    'tanggal'               => $f['tanggal'][$i]
                );
                $this->db->where('id', $f['progres_id'][$i]);
                $this->db->update('t_sewing_progres', $update);
            }
        }

        $result['result'] = true;
        $result['msg'] = 'Data berhasil disimpan.';

        return $result;
    }

}
