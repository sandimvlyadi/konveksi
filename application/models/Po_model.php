<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.*, b.`nama_po`, c.`nama_customer` FROM `t_po` a LEFT JOIN `m_po` b ON a.`id_m_po` = b.`id` LEFT JOIN `m_customer` c ON a.`id_m_customer` = c.`id` ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE a.`nomor` LIKE '%". $s ."%' OR a.`tanggal` LIKE '%". $s ."%' OR a.`jatuh_tempo` LIKE '%". $s ."%' OR b.`nama_po` LIKE '%". $s ."%' OR c.`nama_customer` LIKE '%". $s ."%' OR a.`ukuran_s` LIKE '%". $s ."%' OR a.`ukuran_m` LIKE '%". $s ."%' OR a.`ukuran_l` LIKE '%". $s ."%' OR a.`ukuran_xl` LIKE '%". $s ."%' OR a.`ukuran_xxl` LIKE '%". $s ."%' OR a.`jumlah` LIKE '%". $s ."%' OR a.`harga` LIKE '%". $s ."%' OR a.`total` LIKE '%". $s ."%' OR a.`dp` LIKE '%". $s ."%' OR a.`pelunasan` LIKE '%". $s ."%' AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'nama_po') {
                    $q .= "ORDER BY b.`". $col ."` ". $dir ." ";
                } elseif ($col == 'nama_customer') {
                    $q .= "ORDER BY c.`". $col ."` ". $dir ." ";
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data po tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*,
                    b.`nama_po`,
                    c.`nama_customer`
                FROM
                    `t_po` a
                LEFT JOIN
                    `m_po` b
                        ON
                    a.`id_m_po` = b.`id`
                LEFT JOIN
                    `m_customer` c
                        ON
                    a.`id_m_customer` = c.`id`
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r[0];
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        $q = '';
        if ($id == 0) {
            $q =    "INSERT INTO
                        `t_po`
                        (
                            `created_at`,
                            `nomor`,
                            `tanggal`,
                            `jatuh_tempo`,
                            `id_m_po`,
                            `id_m_customer`,
                            `ukuran_s`,
                            `ukuran_m`,
                            `ukuran_l`,
                            `ukuran_xl`,
                            `ukuran_xxl`,
                            `jumlah`,
                            `harga`,
                            `total`,
                            `dp`,
                            `pelunasan`,
                            `keterangan`
                        )
                    VALUES
                        (
                            NOW(),
                            '". $this->db->escape_str($f['nomor']) ."',
                            '". $this->db->escape_str($f['tanggal']) ."',
                            '". $this->db->escape_str($f['jatuh_tempo']) ."',
                            '". $this->db->escape_str($f['id_m_po']) ."',
                            '". $this->db->escape_str($f['id_m_customer']) ."',
                            '". $this->db->escape_str($f['ukuran_s']) ."',
                            '". $this->db->escape_str($f['ukuran_m']) ."',
                            '". $this->db->escape_str($f['ukuran_l']) ."',
                            '". $this->db->escape_str($f['ukuran_xl']) ."',
                            '". $this->db->escape_str($f['ukuran_xxl']) ."',
                            '". $this->db->escape_str($f['jumlah']) ."',
                            '". $this->db->escape_str($f['harga']) ."',
                            '". $this->db->escape_str($f['total']) ."',
                            '". $this->db->escape_str($f['dp']) ."',
                            '". $this->db->escape_str($f['pelunasan']) ."',
                            '". $this->db->escape_str($f['keterangan']) ."'
                        )
                    ;";
        } else{
            $q =    "UPDATE
                        `t_po`
                    SET
                        `modified_at` = NOW(),
                        `nomor` = '". $this->db->escape_str($f['nomor']) ."',
                        `tanggal` = '". $this->db->escape_str($f['tanggal']) ."',
                        `jatuh_tempo` = '". $this->db->escape_str($f['jatuh_tempo']) ."',
                        `id_m_po` = '". $this->db->escape_str($f['id_m_po']) ."',
                        `id_m_customer` = '". $this->db->escape_str($f['id_m_customer']) ."',
                        `ukuran_s` = '". $this->db->escape_str($f['ukuran_s']) ."',
                        `ukuran_m` = '". $this->db->escape_str($f['ukuran_m']) ."',
                        `ukuran_l` = '". $this->db->escape_str($f['ukuran_l']) ."',
                        `ukuran_xl` = '". $this->db->escape_str($f['ukuran_xl']) ."',
                        `ukuran_xxl` = '". $this->db->escape_str($f['ukuran_xxl']) ."',
                        `jumlah` = '". $this->db->escape_str($f['jumlah']) ."',
                        `harga` = '". $this->db->escape_str($f['harga']) ."',
                        `total` = '". $this->db->escape_str($f['total']) ."',
                        `dp` = '". $this->db->escape_str($f['dp']) ."',
                        `pelunasan` = '". $this->db->escape_str($f['pelunasan']) ."',
                        `keterangan` = '". $this->db->escape_str($f['keterangan']) ."'
                    WHERE
                        `id` = '". $this->db->escape_str($id) ."'
                    ;";
        }

        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menyimpan data.';
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_po` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_po`;";
        } else{
            $q = "SELECT * FROM `t_po` WHERE `id` = '". $this->db->escape_str($id) ."';";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
