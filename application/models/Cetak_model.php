<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_model extends CI_Model {

    function purchase_order($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_warna
            ');
            $this->db->from('t_purchase_order_detail a');
            $this->db->join('m_warna b', 'a.id_m_warna = b.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $result['detail'] = $get->result();
        }

        return $result;
    }

    function kebutuhan($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_bahan_baku,
                c.nama_satuan
            ');
            $this->db->from('t_purchase_order_kebutuhan a');
            $this->db->join('m_bahan_baku b', 'a.id_m_bahan_baku = b.id', 'left');
            $this->db->join('m_satuan c', 'a.id_m_satuan = c.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $result['detail'] = $get->result();
        }

        return $result;
    }

    function belanja($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_bahan_baku,
                c.nama_satuan
            ');
            $this->db->from('t_purchase_order_purchasing a');
            $this->db->join('m_bahan_baku b', 'a.id_m_bahan_baku = b.id', 'left');
            $this->db->join('m_satuan c', 'a.id_m_satuan = c.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $result['detail'] = $get->result();
        }

        return $result;
    }


    function invoice($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            a.id,
            a.id_t_po,
            b.nomor,
            b.tanggal,
            b.nama_item,
            b.total,
            b.dp,
            b.pelunasan,
            c.nama_customer,
            c.alamat,
            c.kontak,
            d.nama_payment
        ');
        $this->db->from('t_packing a');
        $this->db->join('t_purchase_order b', 'a.id_t_po = b.id', 'left');
        $this->db->join('m_customer c', 'b.id_m_customer = c.id', 'left');
        $this->db->join('m_payment d', 'b.id_m_payment = d.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_warna
            ');
            $this->db->from('t_purchase_order_detail a');
            $this->db->join('m_warna b', 'a.id_m_warna = b.id', 'left');
            $this->db->where('a.id_t_purchase_order', $result['data']->id_t_po);
            $get = $this->db->get();
            $result['detail'] = $get->result();
        }

        return $result;
    }

    function surat_jalan($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->where('id', $id);
        $this->db->where('deleted_at IS NULL', NULL, FALSE);
        $get = $this->db->get('t_surat_jalan');

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->where('id_t_surat_jalan', $id);
            $get = $this->db->get('t_surat_jalan_detail');
            $result['detail'] = $get->result();
        }

        return $result;
    }

    function biaya_bulanan($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->where('id', $id);
        $this->db->where('deleted_at IS NULL', NULL, FALSE);
        $get = $this->db->get('t_biaya_bulanan');

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->where('id', $id);
            $get = $this->db->get('t_biaya_bulanan');
            $result['detail'] = $get->result();

        }

        return $result;
    }

    function naik_jahit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_warna
            ');
            $this->db->from('t_purchase_order_detail a');
            $this->db->join('m_warna b', 'a.id_m_warna = b.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $detail = $get->result();

            $tanggal = array();
            for ($i=0; $i < count($detail); $i++) { 
                $this->db->select_sum('ukuran_s');
                $this->db->select_sum('ukuran_m');
                $this->db->select_sum('ukuran_l');
                $this->db->select_sum('ukuran_xl');
                $this->db->select_sum('ukuran_xxl');
                $this->db->select('SUM(ukuran_s) + SUM(ukuran_m) + SUM(ukuran_l) + SUM(ukuran_xl) + SUM(ukuran_xxl) as total', FALSE);
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_cutting_progres')->result();
                $detail[$i]->cutting = $get;

                $this->db->select('*');
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_naik_jahit_progres')->result();
                $detail[$i]->progres = $get;

                foreach ($get as $key => $value) {
                    if (!in_array($value->tanggal, $tanggal)) {
                        array_push($tanggal, $value->tanggal);
                    }
                }
            }

            $result['detail'] = $detail;
            $result['tanggal'] = $tanggal;
        }

        return $result;
    }

    function qc($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_warna
            ');
            $this->db->from('t_purchase_order_detail a');
            $this->db->join('m_warna b', 'a.id_m_warna = b.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $detail = $get->result();

            $tanggal = array();
            for ($i=0; $i < count($detail); $i++) { 
                $this->db->select_sum('ukuran_s');
                $this->db->select_sum('ukuran_m');
                $this->db->select_sum('ukuran_l');
                $this->db->select_sum('ukuran_xl');
                $this->db->select_sum('ukuran_xxl');
                $this->db->select('SUM(ukuran_s) + SUM(ukuran_m) + SUM(ukuran_l) + SUM(ukuran_xl) + SUM(ukuran_xxl) as total', FALSE);
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_cutting_progres')->result();
                $detail[$i]->cutting = $get;

                $this->db->select('*');
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_qc_progres')->result();
                $detail[$i]->progres = $get;

                foreach ($get as $key => $value) {
                    if (!in_array($value->tanggal, $tanggal)) {
                        array_push($tanggal, $value->tanggal);
                    }
                }
            }

            $result['detail'] = $detail;
            $result['tanggal'] = $tanggal;
        }

        return $result;
    }

    function cutting($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_warna
            ');
            $this->db->from('t_purchase_order_detail a');
            $this->db->join('m_warna b', 'a.id_m_warna = b.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $detail = $get->result();

            $tanggal = array();
            for ($i=0; $i < count($detail); $i++) { 
                $this->db->select_sum('ukuran_s');
                $this->db->select_sum('ukuran_m');
                $this->db->select_sum('ukuran_l');
                $this->db->select_sum('ukuran_xl');
                $this->db->select_sum('ukuran_xxl');
                $this->db->select('SUM(ukuran_s) + SUM(ukuran_m) + SUM(ukuran_l) + SUM(ukuran_xl) + SUM(ukuran_xxl) as total', FALSE);
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_purchase_order_detail')->result();
                $detail[$i]->cutting = $get;

                $this->db->select('*');
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_cutting_progres')->result();
                $detail[$i]->progres = $get;

                foreach ($get as $key => $value) {
                    if (!in_array($value->tanggal, $tanggal)) {
                        array_push($tanggal, $value->tanggal);
                    }
                }
            }

            $result['detail'] = $detail;
            $result['tanggal'] = $tanggal;
        }

        return $result;
    }

    function packing($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('
            *
        ');
        $this->db->from('t_purchase_order a');
        $this->db->join('m_customer b', 'a.id_m_customer = b.id', 'left');
        $this->db->where('a.id', $id);
        $get = $this->db->get();

        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result()[0];

            $this->db->select('
                a.*,
                b.nama_warna
            ');
            $this->db->from('t_purchase_order_detail a');
            $this->db->join('m_warna b', 'a.id_m_warna = b.id', 'left');
            $this->db->where('a.id_t_purchase_order', $id);
            $get = $this->db->get();
            $detail = $get->result();

            $tanggal = array();
            for ($i=0; $i < count($detail); $i++) { 
                $this->db->select_sum('ukuran_s');
                $this->db->select_sum('ukuran_m');
                $this->db->select_sum('ukuran_l');
                $this->db->select_sum('ukuran_xl');
                $this->db->select_sum('ukuran_xxl');
                $this->db->select('SUM(ukuran_s) + SUM(ukuran_m) + SUM(ukuran_l) + SUM(ukuran_xl) + SUM(ukuran_xxl) as total', FALSE);
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_qc_progres')->result();
                $detail[$i]->cutting = $get;

                $this->db->select('*');
                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $detail[$i]->id_m_warna);
                $get = $this->db->get('t_packing_progres')->result();
                $detail[$i]->progres = $get;

                foreach ($get as $key => $value) {
                    if (!in_array($value->tanggal, $tanggal)) {
                        array_push($tanggal, $value->tanggal);
                    }
                }
            }

            $result['detail'] = $detail;
            $result['tanggal'] = $tanggal;
        }

        return $result;
    }
}
