<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order_model extends CI_Model {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_model', 'setting');
	}

    function _get($data = array())
    {
        $q = "SELECT a.*, b.`nama_payment`, c.`nama_customer` FROM `t_purchase_order` a LEFT JOIN `m_payment` b ON a.`id_m_payment` = b.`id` LEFT JOIN `m_customer` c ON a.`id_m_customer` = c.`id` ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`nomor` LIKE '%". $s ."%' OR a.`tanggal` LIKE '%". $s ."%' OR b.`nama_payment` LIKE '%". $s ."%' OR c.`nama_payment` LIKE '%". $s ."%' OR a.`nama_item` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'nama_payment' || $col == 'jumlah') {
                    $q .= "ORDER BY b.`". $col ."` ". $dir ." ";
                } elseif ($col == 'nama_customer') {
                    $q .= "ORDER BY c.`". $col ."` ". $dir ." ";
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data purchase order ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*
                FROM
                    `t_purchase_order` a
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $this->db->where('id_t_purchase_order', $id);
            $warna = $this->db->get('t_purchase_order_detail');

            $result['result'] = true;
            $result['data'] = $r[0];
            $result['warna'] = $warna->result();
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        if (intval($f['id_m_customer']) == 0) {
            $newCustomer = array(
                'created_at' => date('Y-m-d H:i:s'),
                'nama_customer' => $f['nama_customer'],
                'alamat' => $f['alamat_customer'],
                'kontak' => $f['kontak_customer'],
                'keterangan' => $f['keterangan_customer']
            );

            $f['id_m_customer'] = $this->setting->store_customer($newCustomer);
        }

        $totalPembayaran = $f['total_pembayaran'];
        $totalPembayaran = str_replace('Rp. ', '', $totalPembayaran);
        $totalPembayaran = str_replace('.', '', $totalPembayaran);
        $dp = $f['dp'];
        $dp = str_replace('Rp. ', '', $dp);
        $dp = str_replace('.', '', $dp);
        if ($id == 0) {
            $insert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'nomor' => $f['nomor'],
                'tanggal' => $f['tanggal'],
                // 'id_m_payment' => $f['id_m_payment'],
                'id_m_customer' => $f['id_m_customer'],
                'nama_item' => $f['nama_item'],
                'jumlah' => $f['total_qty'],
                'total' => $totalPembayaran,
                'dp' => $dp,
                'pelunasan' => intval($totalPembayaran) - intval($dp),
                'keterangan' => $f['keterangan']
            );
            $this->db->insert('t_purchase_order', $insert);
            $id = $this->db->insert_id();

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            for ($i=0; $i < count($f['id_m_warna']); $i++) { 
                if (intval($f['id_m_warna'][$i]) == 0) {
                    $newWarna = array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'nama_warna' => $f['id_m_warna'][$i]
                    );
                    $f['id_m_warna'][$i] = $this->setting->store_warna($newWarna);
                }
                $id_warna = $f['id_m_warna'][$i];
                $ukuran_s = $f['ukuran_s'][$i];
                $ukuran_m = $f['ukuran_m'][$i];
                $ukuran_l = $f['ukuran_l'][$i];
                $ukuran_xl = $f['ukuran_xl'][$i];
                $ukuran_xxl = $f['ukuran_xxl'][$i];
                $qty = $f['qty'][$i];
                $harga = $f['harga'][$i];
                $harga = str_replace('Rp. ', '', $harga);
                $harga = str_replace('.', '', $harga);
                $total = intval($qty) * intval($harga);
                $keterangan = $f['keterangan_warna'][$i];

                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_purchase_order' => $id,
                    'id_m_warna' => $id_warna,
                    'ukuran_s' => $ukuran_s,
                    'ukuran_m' => $ukuran_m,
                    'ukuran_l' => $ukuran_l,
                    'ukuran_xl' => $ukuran_xl,
                    'ukuran_xxl' => $ukuran_xxl,
                    'qty' => $qty,
                    'harga' => $harga,
                    'total' => $total,
                    'keterangan' => $keterangan
                );
                $this->db->insert('t_purchase_order_detail', $insert);
            }
        } else{
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'nomor' => $f['nomor'],
                'tanggal' => $f['tanggal'],
                // 'id_m_payment' => $f['id_m_payment'],
                'id_m_customer' => $f['id_m_customer'],
                'nama_item' => $f['nama_item'],
                'jumlah' => $f['total_qty'],
                'total' => $totalPembayaran,
                'dp' => $dp,
                'pelunasan' => intval($totalPembayaran) - intval($dp),
                'keterangan' => $f['keterangan']
            );
            $this->db->where('id', $id);
            $this->db->update('t_purchase_order', $update);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            $this->db->where('id_t_purchase_order', $id);
            $this->db->where_not_in('id_m_warna', $f['id_m_warna']);
            $this->db->delete('t_purchase_order_detail');

            for ($i=0; $i < count($f['id_m_warna']); $i++) { 
                if (intval($f['id_m_warna'][$i]) == 0) {
                    $newWarna = array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'nama_warna' => $f['id_m_warna'][$i]
                    );
                    $f['id_m_warna'][$i] = $this->setting->store_warna($newWarna);
                }
                $id_warna = $f['id_m_warna'][$i];
                $ukuran_s = $f['ukuran_s'][$i];
                $ukuran_m = $f['ukuran_m'][$i];
                $ukuran_l = $f['ukuran_l'][$i];
                $ukuran_xl = $f['ukuran_xl'][$i];
                $ukuran_xxl = $f['ukuran_xxl'][$i];
                $qty = $f['qty'][$i];
                $harga = $f['harga'][$i];
                $harga = str_replace('Rp. ', '', $harga);
                $harga = str_replace('.', '', $harga);
                $total = intval($qty) * intval($harga);
                $keterangan = $f['keterangan_warna'][$i];

                $this->db->where('id_t_purchase_order', $id);
                $this->db->where('id_m_warna', $id_warna);
                $get = $this->db->get('t_purchase_order_detail');
                if ($get->num_rows() > 0) {
                    $update = array(
                        'modified_at' => date('Y-m-d H:i:s'),
                        'ukuran_s' => $ukuran_s,
                        'ukuran_m' => $ukuran_m,
                        'ukuran_l' => $ukuran_l,
                        'ukuran_xl' => $ukuran_xl,
                        'ukuran_xxl' => $ukuran_xxl,
                        'qty' => $qty,
                        'harga' => $harga,
                        'total' => $total,
                        'keterangan' => $keterangan
                    );
                    $this->db->where('id_t_purchase_order', $id);
                    $this->db->where('id_m_warna', $id_warna);
                    $this->db->update('t_purchase_order_detail', $update);
                } else{
                    $insert = array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'id_t_purchase_order' => $id,
                        'id_m_warna' => $id_warna,
                        'ukuran_s' => $ukuran_s,
                        'ukuran_m' => $ukuran_m,
                        'ukuran_l' => $ukuran_l,
                        'ukuran_xl' => $ukuran_xl,
                        'ukuran_xxl' => $ukuran_xxl,
                        'qty' => $qty,
                        'harga' => $harga,
                        'total' => $total,
                        'keterangan' => $keterangan
                    );
                    $this->db->insert('t_purchase_order_detail', $insert);
                }
            }
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_purchase_order` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_purchase_order` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_purchase_order` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

    function select_item()
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $this->db->select('nama_item');
        $this->db->where('deleted_at IS NULL', NULL, FALSE);
        $this->db->group_by('nama_item');
        $get = $this->db->get('t_purchase_order');
        if ($get->num_rows() > 0) {
            $result['result'] = true;
            $result['data'] = $get->result();
        }

        return $result;
    }

    function detail($id = 0)
    {
        $this->db->select('t_purchase_order_detail.*, nama_warna');
        $this->db->join('m_warna', 't_purchase_order_detail.id_m_warna = m_warna.id', 'left');
        $this->db->where('id_t_purchase_order', $id);
        $result = $this->db->get('t_purchase_order_detail')->result();

        return $result;
    }

}