<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_barang_jadi_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.* FROM `t_gudang_barang_jadi` a ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (a.`qty` LIKE '%". $s ."%' OR a.`tanggal` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%' OR a.`nama_item` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL ";
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data barang ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*
                FROM
                    `t_gudang_barang_jadi` a
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r[0];
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        if ($id == 0) {
            $insert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'nama_item' => $f['nama_item'],
                'qty' => $f['qty'],
                'tanggal' => $f['tanggal'],
                'keterangan' => $f['keterangan']
            );
            $this->db->insert('t_gudang_barang_jadi', $insert);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';
        } else{
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'nama_item' => $f['nama_item'],
                'qty' => $f['qty'],
                'tanggal' => $f['tanggal'],
                'keterangan' => $f['keterangan']
            );
            $this->db->where('id', $id);
            $this->db->update('t_gudang_barang_jadi', $update);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_gudang_barang_jadi` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_gudang_barang_jadi` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_gudang_barang_jadi` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
