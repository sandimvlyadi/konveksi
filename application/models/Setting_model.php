<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

    function store_customer($data = array())
    {
        $this->db->where('nama_customer', $data['nama_customer']);
        $get = $this->db->get('m_customer');
        if ($get->num_rows() > 0) {
            $customer = $get->result();
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'nama_customer' => $data['nama_customer'],
                'alamat' => $data['alamat'],
                'keterangan' => $data['keterangan']
            );
            $this->db->where('id', $customer[0]->id);
            $this->db->update('m_customer', $update);

            return $customer[0]->id;
        } else{
            $this->db->insert('m_customer', $data);
            return $this->db->insert_id();
        }
    }

    function store_warna($data = array())
    {
        $this->db->where('nama_warna', $data['nama_warna']);
        $get = $this->db->get('m_warna');
        if ($get->num_rows() > 0) {
            $warna = $get->result();
            return $warna[0]->id;
        } else{
            $this->db->insert('m_warna', $data);
            return $this->db->insert_id();
        }
    }

    function store_bahan_baku($data = array())
    {
        $this->db->where('nama_bahan_baku', $data['nama_bahan_baku']);
        $get = $this->db->get('m_bahan_baku');
        if ($get->num_rows() > 0) {
            $bahan_baku = $get->result();
            return $bahan_baku[0]->id;
        } else{
            $this->db->insert('m_bahan_baku', $data);
            return $this->db->insert_id();
        }
    }

    function store_satuan($data = array())
    {
        $this->db->where('nama_satuan', $data['nama_satuan']);
        $get = $this->db->get('m_satuan');
        if ($get->num_rows() > 0) {
            $satuan = $get->result();
            return $satuan[0]->id;
        } else{
            $this->db->insert('m_satuan', $data);
            return $this->db->insert_id();
        }
    }

}