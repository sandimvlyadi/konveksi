<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.*, b.`nomor`FROM `t_invoice` a LEFT JOIN `t_po` b ON a.`id_t_po` = b.`id` ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE (b.`nomor` LIKE '%". $s ."%' OR `jumlah` LIKE '%". $s ."%' OR a.`keterangan` LIKE '%". $s ."%') AND a.`deleted_at` IS NULL AND c.`deleted_at` IS NULL ";
        } else{
            $q .= "WHERE a.`deleted_at` IS NULL AND c.`deleted_at` IS NULL ";
        }

        $q .= "GROUP BY a.`id` ";

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'nomor' || $col == 'jumlah') {
                    $q .= "ORDER BY b.`". $col ."` ". $dir ." ";
                } elseif ($col == 'jumlah_selesai') {
                    # code...
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            } else{
                $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
            $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data kebutuhan untuk po ini tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*,
                    b.`nomor`
                FROM
                    `t_kebutuhan` a
                LEFT JOIN
                    `t_po` b
                        ON
                    a.`id_t_po` = b.`id`
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                        AND
                    a.`deleted_at` IS NULL
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $this->db->where('id_t_kebutuhan', $id);
            $bahan_baku = $this->db->get('t_kebutuhan_detail');

            $result['result'] = true;
            $result['data'] = $r[0];
            $result['bahan_baku'] = $bahan_baku->result();
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        if ($id == 0) {
            $insert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'id_t_po' => $f['id_t_po'],
                'keterangan' => $f['keterangan']
            );
            $this->db->insert('t_kebutuhan', $insert);
            $id = $this->db->insert_id();

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            for ($i=0; $i < count($f['id_m_bahan_baku']); $i++) { 
                $id_bahan_baku = $f['id_m_bahan_baku'][$i];
                $qty = $f['qty'][$i];
                $harga = $f['harga'][$i];
                $jumlah = intval($qty) * intval($harga);
                $keterangan = $f['keterangan_bahan_baku'][$i];

                $insert = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_t_kebutuhan' => $id,
                    'id_m_bahan_baku' => $id_bahan_baku,
                    'qty' => $qty,
                    'harga' => $harga,
                    'jumlah' => $jumlah,
                    'keterangan' => $keterangan
                );
                $this->db->insert('t_kebutuhan_detail', $insert);
            }
        } else{
            $update = array(
                'modified_at' => date('Y-m-d H:i:s'),
                'id_t_po' => $f['id_t_po'],
                'keterangan' => $f['keterangan']
            );
            $this->db->where('id', $id);
            $this->db->update('t_kebutuhan', $update);

            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';

            $this->db->where('id_t_kebutuhan', $id);
            $this->db->where_not_in('id_m_bahan_baku', $f['id_m_bahan_baku']);
            $this->db->delete('t_kebutuhan_detail');

            for ($i=0; $i < count($f['id_m_bahan_baku']); $i++) { 
                $id_bahan_baku = $f['id_m_bahan_baku'][$i];
                $qty = $f['qty'][$i];
                $harga = $f['harga'][$i];
                $jumlah = intval($qty) * intval($harga);
                $keterangan = $f['keterangan_bahan_baku'][$i];

                $this->db->where('id_t_kebutuhan', $id);
                $this->db->where('id_m_bahan_baku', $id_bahan_baku);
                $get = $this->db->get('t_kebutuhan_detail');
                if ($get->num_rows() > 0) {
                    $update = array(
                        'modified_at' => date('Y-m-d H:i:s'),
                        'qty' => $qty,
                        'harga' => $harga,
                        'jumlah' => $jumlah,
                        'keterangan' => $keterangan
                    );
                    $this->db->where('id_t_kebutuhan', $id);
                    $this->db->where('id_m_bahan_baku', $id_bahan_baku);
                    $this->db->update('t_kebutuhan_detail', $update);
                } else{
                    $insert = array(
                        'created_at' => date('Y-m-d H:i:s'),
                        'id_t_kebutuhan' => $id,
                        'id_m_bahan_baku' => $id_bahan_baku,
                        'qty' => $qty,
                        'harga' => $harga,
                        'jumlah' => $jumlah,
                        'keterangan' => $keterangan
                    );
                    $this->db->insert('t_kebutuhan_detail', $insert);
                }
            }
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "UPDATE `t_kebutuhan` SET `deleted_at` = NOW() WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_kebutuhan` WHERE `deleted_at` IS NULL;";
        } else{
            $q = "SELECT * FROM `t_kebutuhan` WHERE `id` = '". $this->db->escape_str($id) ."' AND `deleted_at` IS NULL;";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
