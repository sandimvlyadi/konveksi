<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cutting_detail_model extends CI_Model {

    function _get($data = array())
    {
        $q = "SELECT a.*, b.`nomor`, c.`nama_penjahit` 
              FROM `t_cutting_detail` a 
              LEFT JOIN `t_po` b ON a.`id_t_po` = b.`id`
              LEFT JOIN `m_penjahit` c ON a.`id_m_penjahit` = c.`id` ";

        if ($data['search']['value'] && !isset($data['all'])) {
            $s = $this->db->escape_str($data['search']['value']);
            $q .= "WHERE b.`nomor` LIKE '%". $s ."%' OR
                         c.`nama_penjahit` LIKE '%". $s ."%' OR
                         a.`ukuran_s` LIKE '%". $s ."%' OR
                         a.`ukuran_m` LIKE '%". $s ."%' OR
                         a.`ukuran_l` LIKE '%". $s ."%' OR                         
                         a.`keterangan` LIKE '%". $s ."%' ";
        } else{
            
        }

        if (isset($data['order'])) {
            $dir = $this->db->escape_str($data['order'][0]['dir']);
            $col = $this->db->escape_str($data['columns'][$data['order'][0]['column']]['data']);
            if ($data['order'][0]['column'] != 0) {
                if ($col == 'nomor') {
                    $q .= "ORDER BY b.`". $col ."` ". $dir ." ";
                } elseif ($col == 'nama_penjahit') {
                    $q .= "ORDER BY c.`". $col ."` ". $dir ." ";
                } else{
                    $q .= "ORDER BY a.`". $col ."` ". $dir ." ";
                }
            }else{
              $q .= "ORDER BY a.`id` ". $dir ." ";
            }
        } else{
          $q .= "ORDER BY a.`id` DESC ";
        }

        return $q;
    }

    function _list($data = array())
    {
        $q = $this->_get($data);
        $q .= "LIMIT ". $this->db->escape_str($data['start']) .", ". $this->db->escape_str($data['length']);
        $r = $this->db->query($q, false)->result_array();

        return $r;
    }

    function _filtered($data = array())
    {
        $q = $this->_get($data);
        $r = $this->db->query($q, false)->result_array();

        return count($r);
    }

    function _all($data = array())
    {
        $data['all'] = true;
        $q = $this->_get($data);
        $r = $this->db->query($q)->result_array();

        return count($r);
    }

    function datatable($data = array())
    {
        $result = array(
            'draw'              => 1,
            'recordsTotal'      => 0,
            'recordsFiltered'   => 0,
            'data'              => array(),
            'result'            => false,
            'msg'               => ''
        );

        $list = $this->_list($data);
        if (count($list) > 0) {
            $result = array(
                'draw'              => $data['draw'],
                'recordsTotal'      => $this->_all($data),
                'recordsFiltered'   => $this->_filtered($data),
                'data'              => $list,
                'result'            => true,
                'msg'               => 'Loaded.',
                'start'             => (int) $data['start'] + 1
            );
        } else{
            $result['msg'] = 'No data left.';
        }

        return $result;
    }

    function edit($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => 'Data cutting detail tidak ditemukan.'
        );

        $q =    "SELECT
                    a.*,
                    b.`nomor`,
                    c.`nama_penjahit`
                FROM
                    `t_cutting_detail` a
                LEFT JOIN
                    `t_po` b
                        ON
                    a.`id_t_po` = b.`id`
                LEFT JOIN
                    `m_penjahit` c
                        ON
                    a.`id_m_penjahit` = c.`id`
                WHERE
                    a.`id` = '". $this->db->escape_str($id) ."'
                ;";
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r[0];
        }

        return $result;
    }

    function save($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        parse_str($d['form'], $f);

        $q = '';
        if ($id == 0) {
            $q =    "INSERT INTO
                        `t_cutting_detail`
                        (
                            `id_t_po`,
                            `id_m_penjahit`,
                            `ukuran_s`,
                            `ukuran_m`,
                            `ukuran_l`,
                            `keterangan`
                        )
                    VALUES
                        (
                            '". $this->db->escape_str($f['id_t_po']) ."',
                            '". $this->db->escape_str($f['id_m_penjahit']) ."',
                            '". $this->db->escape_str($f['ukuran_s']) ."',
                            '". $this->db->escape_str($f['ukuran_m']) ."',
                            '". $this->db->escape_str($f['ukuran_l']) ."',
                            '". $this->db->escape_str($f['keterangan']) ."'
                        )
                    ;";
        } else{
            $q =    "UPDATE
                        `t_cutting_detail`
                    SET
                        `id_t_po` = '". $this->db->escape_str($f['id_t_po']) ."',
                        `id_m_penjahit` = '". $this->db->escape_str($f['id_m_penjahit']) ."',
                        `ukuran_s` = '". $this->db->escape_str($f['ukuran_s']) ."',
                        `ukuran_m` = '". $this->db->escape_str($f['ukuran_m']) ."',
                        `ukuran_l` = '". $this->db->escape_str($f['ukuran_l']) ."',
                        `keterangan` = '". $this->db->escape_str($f['keterangan']) ."'
                    WHERE
                        `id` = '". $this->db->escape_str($id) ."'
                    ;";
        }

        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil disimpan.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menyimpan data.';
        }

        return $result;
    }

    function delete($data = array())
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $u = $data['userData'];
        $d = $data['postData'];
        $id = $d['id'];
        $q = "DELETE FROM `t_cutting_detail` WHERE `id` = '". $this->db->escape_str($id) ."';";
        if ($this->db->simple_query($q)) {
            $result['result'] = true;
            $result['msg'] = 'Data berhasil dihapus.';
        } else{
            $result['msg'] = 'Terjadi kesalahan saat menghapus data.';
        }

        return $result;
    }

    function select($id = 0)
    {
        $result = array(
            'result'    => false,
            'msg'       => ''
        );

        $q = "";
        if ($id == 0) {
            $q = "SELECT * FROM `t_cutting_detail`;";
        } else{
            $q = "SELECT * FROM `t_cutting_detail` WHERE `id` = '". $this->db->escape_str($id) ."';";
        }
        $r = $this->db->query($q)->result_array();
        if (count($r) > 0) {
            $result['result'] = true;
            $result['data'] = $r;

            if (count($r) == 1 && $id != 0) {
                $result['data'] = $r[0];
            }
        }

        return $result;
    }

}
